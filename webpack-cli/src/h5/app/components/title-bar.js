import React from 'react';

class TitleBar extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <div className="titlebar">
                <h1 className="title">TitleBar</h1>
            </div>
        );
    };
};

export default TitleBar;