import './index.scss';

import React from 'react';
import { Link, hashHistory } from 'react-router';

class AppPage extends React.Component {
    constructor (props) {
        super(props);
    };

    render () {
        return (
            <div className="app-page">
                {this.props.children}
            </div>
        );
    };
    componentDidMount () {
    };
};

export default AppPage;