import List from './module/list.js';
import Intro from './module/intro.js';

export default (store) => ({
    path: '/app',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./index.js').default)
        }, 'app')
    },
    childRoutes: [
        {
            path: 'list/:id',
            component: List
        },
        {
            path: 'intro/:id',
            component: Intro
        }
    ]
})

