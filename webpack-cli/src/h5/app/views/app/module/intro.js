import React from 'react';
import { Router, hashHistory, Link } from 'react-router';

import TitleBar from 'componentPath/title-bar.js';

import sliderDefault from 'imgPath/slider-default.jpg';

class AppIntro extends React.Component {
    constructor(props) {
        super(props);

        console.log(props)
        this.state = {
            id: props.params.id
        };
    };

    render() {
        return (
            <div className="appintro">
                <h1 className="title">AppIntro</h1>
                <TitleBar />
                <p className="text">
                    <span>id ={this.state.id}</span>
                </p>
                <p className="text">
                    <span>go to home</span>
                    <Link className="link-btn" to={`/`}>button</Link>
                </p>
                <img src={sliderDefault} alt=""/>
            </div>
        );
    };
};

export default AppIntro;

