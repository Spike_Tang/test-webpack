import React from 'react';
import {Link, Router, hashHistory } from 'react-router';

import TitleBar from '../../../components/title-bar.js';

class AppList extends React.Component {
    constructor (props) {
        super(props);

        console.log(props)
        this.state = {
            id: props.params.id
        }
    };

    render() {
        return (
            <div className="applist">
                <h1 className="title">AppList</h1>
                <TitleBar />
                <p className="text">
                    <span>id ={this.state.id}</span>
                </p>
                <p className="text">
                    <span>go to home</span>
                    <Link className="link-btn" to={`/`}>button</Link>
                </p>
            </div>
        );
    };
};

export default AppList;