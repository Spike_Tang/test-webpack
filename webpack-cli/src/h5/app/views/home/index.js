import './index.scss';

import React from 'react';
import { Link, hashHistory } from 'react-router';

import TitleBar from '../../components/title-bar.js';

class Home extends React.Component {
    constructor (props) {
        super(props);
    };

    render () {
        return (
            <div className="home">
                <h1 className="title">home</h1>
                <TitleBar />
                <p className="text">
                    <span>go to /app/list/123</span>
                    <Link className="link-btn" to={`/app/list/123`}>button12</Link>
                </p>
                <p className="text">
                    <span>go to /app/intro/123</span>
                    <Link className="link-btn" to={`/app/intro/123`}>button</Link>
                </p>
            </div>
        );
    };

    componentDidMount () {
    };
};

export default Home;