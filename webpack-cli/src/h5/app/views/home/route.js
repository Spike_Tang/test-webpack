export const IndexHome = (store) => ({
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./index.js').default)
        }, 'home')
    }
});

export const Home = (store) => ({
    path: '/home',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./index.js').default)
        }, 'home')
    }
});

