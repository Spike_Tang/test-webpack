import './style/reset.css';
import './style/common.scss';

import '../assets/adaptive.js';

import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes/routes.js';

ReactDOM.render(
    <Routes />,
	document.getElementById('root')
);

// 热替换HMR，需要加入这段代码才会进行生效
if(module.hot){
    module.hot.accept();
};