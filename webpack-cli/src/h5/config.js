/*
    entryJsName: 入口文件，相对于项目目录的路径。
    rootBuildPath: 文件打包后的根目录，
    buildFilePath: 文件打包后根目录下的子目录名
    test_publicPath: 测试环境下的存放路径
    pro_publicPath: 正式环境下的存放路径
    hasDllReferencePlugin: true 启用dll-plugin 插件
    vendor: 需要添加至静态文件的模块、框架、或者文件
    hasCommonsChunkPlugin: true 启用commons-chunk-plugin 插件

    所有js打包到一个文件中（异步文件除外）：
        hasDllReferencePlugin hasCommonsChunkPlugin都设置为false；
        设置后 CommonsChunkPlugin,DllReferencePlugin 都不会启用。

    启用单独打包静态框架：
        hasDllReferencePlugin需要设置为true，并且vendor 数组不能为空；
        设置后 DllReferencePlugin 会将vendor 中的模块打包进去；

    启用提取复用代码（hasDllReferencePlugin为false并且当框架文件、模块文件在多个文件引用时，也会将框架文件、模块文件提取到这个文件中）：
        hasCommonsChunkPlugin需要设置为true
        设置后 被多处引用的代码会打包进来，在页面初次渲染时会多请求一个 common.js

*/

//填写路径以src子集目录为根目录
const path = require('path');

module.exports = {
    entryJsName: './app/index.js',
    buildFilePath: '/h5',
    test_publicPath: '',
    pro_publicPath: '',
    hasDllReferencePlugin: true,
    vendor: [
        'react',
        'react-dom',
        'react-router',
        'jquery',
        path.resolve(__dirname, './assets/adaptive.js')
    ],
    providePlugin: {},
    hasCommonsChunkPlugin: true
};