

module.exports = (data) => {
    const {
        buildPath,
        environment
    } = data;

    return {
        devtool: environment === 'build'? false: 'inline-source-map',
        devServer: {
            // 开发单页面时开启，所有跳转都将指向 index.html
            historyApiFallback:true,
            host: '0.0.0.0',
            port: 80,
            hot: true,
            disableHostCheck: true,
            contentBase: buildPath
        }
    };
};

