const path = require('path');
const fs = require('fs');

const npmArgument = JSON.parse(process.env.npm_config_argv).original;
const npmArgumentLength = npmArgument.length - 1;
const projectPath = path.resolve(__dirname, `../src${npmArgument[npmArgumentLength].replace(/--/, '')}`);
const projectConfigPath = `${projectPath}/config.js`;

const projectConfig = require(projectConfigPath);
const buildPath = path.join(path.resolve(__dirname, '../build'), projectConfig.buildFilePath);

const filePath = buildPath;
const commonSearchName = /^(common).*(js)$/;
const vendorSearchName = /^(vendor).*(js)$/;

function searchFilePathByName (filePath, searchName) {
    return new Promise((resolve, reject) => {
        fs.readdir(filePath, (err, files) => {
            if(err){
                console.log('要查看的文件路径错误');
                return reject(false);
            };
            let writeFileName = files.filter(function(currentValue, index, arr){
                return searchName.test(currentValue);
            })[0];
            if (!writeFileName) {
                console.log('没找到文件');
                return reject(false);
            };
            resolve(writeFileName);
        });
    });
};

function copyFileText (filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, data) => {
            if(err){
                console.log('要查看的文件路径错误');
                return reject('');
            };
            resolve(data);
        });
    });
};

function removeFile (filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                console.log("删除成功");
                return reject(err);
            };
            resolve();
        });
    });
};

function writeFile (filePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath,
            data,
            {flag: 'a', mode: '0666'},
            function(err){
                if(err){
                    console.log("文件写入失败");
                    return reject(err);
                };
                resolve();
            }
        );
    });
};

const asyncReadFile = async function () {
    let commonFileName = await searchFilePathByName(filePath, commonSearchName).catch((bool) => bool);
    let vendorFileName = await searchFilePathByName(filePath, vendorSearchName).catch((bool) => bool);
    if ( !commonFileName || !vendorFileName) {
        console.log('没有指定文件');
        return;
    };
    let commonFilePath = `${filePath}/${commonFileName}`;
    let vendorFilePath = `${filePath}/${vendorFileName}`;
    let commonFileText = await copyFileText(commonFilePath);
    let vendorFileText = await copyFileText(vendorFilePath);
    await removeFile(commonFilePath);
    // await removeFile(vendorFilePath);
    await writeFile(commonFilePath, commonFileText);
    await writeFile(commonFilePath, vendorFileText);
};

asyncReadFile();