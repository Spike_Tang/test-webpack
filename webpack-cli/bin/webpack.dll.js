const path = require('path');

const webpack = require('webpack');
const merge = require('webpack-merge');
const DllPlugin = require('webpack/lib/DllPlugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyESPlugin = require('uglifyjs-webpack-plugin');

const npmArgument = JSON.parse(process.env.npm_config_argv).original;
console.log('/* -----------------------------dll----------------------------------------- */')
console.log(npmArgument)
const npmArgumentLength = npmArgument.length - 1;
const projectPath = path.resolve(__dirname, `../src${npmArgument[npmArgumentLength].replace(/--/, '')}`);
const projectConfigPath = `${projectPath}/config.js`;

const defaultConfig = require('./base.config.js');
const projectConfig = require(projectConfigPath);
const {
    rootBuildPath,
    buildFilePath,
    vendor
} = merge(defaultConfig, projectConfig);
const buildPath = path.join(rootBuildPath, buildFilePath);

module.exports = {
    // JS 执行入口文件
    entry: {
        // 把 React 相关模块的放到一个单独的动态链接库
        vendor: vendor
        // 把项目需要所有的 polyfill 放到一个单独的动态链接库
        // polyfill: ['core-js/fn/object/assign', 'core-js/fn/promise', 'whatwg-fetch'],
    },
    output: {
        // 输出的动态链接库的文件名称，[name] 代表当前动态链接库的名称，
        // 也就是 entry 中配置的 react 和 polyfill
        filename: '[name].js',
        // 输出的文件都放到 buildPath 目录下
        path: buildPath,
        // 存放动态链接库的全局变量名称，例如对应 react 来说就是 _dll_react
        // 之所以在前面加上 _dll_ 是为了防止全局变量冲突
        library: '[name]',
    },
    plugins: [
    	new CleanWebpackPlugin([
    		`${buildPath}/vendor.js`,
    		`${buildPath}/*.manifest.json`
    	],{
            allowExternal: true// 允许清理除了根目录之外的
        }),
      	// 切换环境,react模块的根据  process.env.NODE_ENV === 'production' 使用生产版  development 开发版
        new webpack.DefinePlugin({
          	'process.env': {
          		NODE_ENV: JSON.stringify("production")
          	}
        }),
        // 接入 DllPlugin
        new DllPlugin({
            // 动态链接库的全局变量名称，需要和 output.library 中保持一致
            // 该字段的值也就是输出的 manifest.json 文件 中 name 字段的值
            // 例如 react.manifest.json 中就有 "name": "_dll_react"
            name: '[name]',
            // 描述动态链接库的 manifest.json 文件输出时的文件名称
            //path: path.join(__dirname, 'buildPath', '[name].manifest.json'),
            path: `${buildPath}/[name].manifest.json`
        }),
        // 压缩
        new UglifyESPlugin({
        	uglifyOptions: {
        		compress: {
    		    	warnings: false,
    		    	drop_console: true,
    		    	collapse_vars: true,
    		    	reduce_vars: true
    		  	},
    		  	output: {
    		        beautify: false,
    		        comments: false,
    		     }
        	}
        })
    ]
};