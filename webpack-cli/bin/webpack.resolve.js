const path = require('path');

module.exports = (data) => {
    const {
        buildFilePath
    } = data;

    return {
        alias:{
            stylePath: path.resolve(__dirname, `../src/${buildFilePath}/app/style`),
            imgPath: path.resolve(__dirname, `../src/${buildFilePath}/app/style/img`),
            componentPath: path.resolve(__dirname, `../src/${buildFilePath}/app/components`)
        },
        extensions: ['.jsx', '.js', '.json', 'css', 'scss']
    };
};

