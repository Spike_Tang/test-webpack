module.exports = (data) => {
    const {
        buildPath,
        test_publicPath,
        pro_publicPath,
        environment
    } = data;

    let name = environment === 'build' ? '[name].[hash].js' : environment === 'test' ? '[name].test.[hash].js' : '[name].dev.js';

    return {
        path: buildPath,
        filename: name,
        chunkFilename: name,
        publicPath: environment === 'build' ? pro_publicPath : environment === 'test' ? test_publicPath : ''
    };
};

