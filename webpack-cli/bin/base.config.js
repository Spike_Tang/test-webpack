/*
    entryJsName: 入口文件，相对于项目目录的路径。
    rootBuildPath: 文件打包后的根目录，
    buildFilePath: 文件打包后根目录下的子目录名
    test_publicPath: 测试环境下的存放路径
    pro_publicPath: 正式环境下的存放路径
    hasDllReferencePlugin: true 启用dll-plugin 插件
    vendor: 需要添加至静态文件的模块、框架、或者文件
    hasCommonsChunkPlugin: true 启用commons-chunk-plugin
*/
const path = require('path');

module.exports = {
    rootBuildPath: path.resolve(__dirname, '../build'),
    entryJsName: './app/index.js',
    buildFilePath: '',
    test_publicPath: '',
    pro_publicPath: '',
    hasDllReferencePlugin: true,
    vendor: [],
    providePlugin: {
        '$': 'jquery',
        'jQuery': 'jquery'
    },
    hasCommonsChunkPlugin: true
};