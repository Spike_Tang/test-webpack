const path = require('path');
const fs = require('fs');

const webpack = require('webpack');
const merge = require('webpack-merge');
const webpackDevServer = require('webpack-dev-server');
const DllReferencePlugin = require('webpack/lib/DllReferencePlugin');

const npmArgument = JSON.parse(process.env.npm_config_argv).original;
const npmArgumentLength = npmArgument.length - 1;
const environment = npmArgument[npmArgumentLength - 1].replace(/--/, '');

const projectPath = path.resolve(__dirname, `../src${npmArgument[npmArgumentLength].replace(/--/, '')}`);
const projectConfigPath = `${projectPath}/config.js`;
const defaultConfig = require('./base.config.js');
const projectConfig = require(projectConfigPath);

const buildPath = path.join(defaultConfig.rootBuildPath, projectConfig.buildFilePath);
// 合并项目配置 和 默认配置
let config = merge(defaultConfig, projectConfig);
config.buildPath = buildPath;
config.environment = environment;
config.projectPath = projectPath;

// 使用webpack node API 启动 vendor 打包
(async function() {
    console.log('开始运行')
    let isVendor = await searchFilePathByName(buildPath, /^(vendor).*(js)$/).catch((bool) => bool);
    console.log(config.hasDllReferencePlugin, isVendor);
    if ( config.hasDllReferencePlugin && isVendor) {
        await runWebpack(require('./webpack.dll.js'));
        console.log('vender准备就绪');
    };
    // 传入配置想 组合生成 webpack.config
    const webpackConfig = {
        entry: {
            index: path.join(projectPath, './app/index.js')
        },
        output: require('./webpack.output.js')(config),
        module: require('./webpack.module.js')(config),
        plugins: require('./webpack.plugin.js')(config),
        devtool: require('./webpack.server.js')(config).devtool,
        resolve: require('./webpack.resolve.js')(config)
    };
    const devOptions = require('./webpack.server.js')(config).devServer;
    console.log('开始判断环境');
    if (environment === 'dev') {
        console.log('开发环境');
        // webpackDevServer.addDevServerEntrypoints 需要在 webpack(webpackConfig) 之前
        webpackDevServer.addDevServerEntrypoints(webpackConfig, devOptions);
        const compiler = webpack(webpackConfig);
        const server = new webpackDevServer(compiler, devOptions);
        server.listen(devOptions.port, devOptions.host, (err) => {
            console.log(`dev server listening on port ${devOptions.port}`);
        });
    }else{
        console.log('生产环境');
        await runWebpack(webpackConfig);
    };
    console.log('运行结束');
})();

function runWebpack(config) {
    return new Promise((resovle, reject) => {
        return webpack(config).run((err, state) => {
            if (err) {
                return reject(err);
            };
            console.log(state.toString({
                // all: 'normal',
                chunks: true,
                chunkModules: true,
                chunkOrigins: true,
                colors: true    // 在控制台展示颜色
            }));
            return resovle(state);
        });
    });
};

function searchFilePathByName (filePath, searchName) {
    return new Promise((resolve, reject) => {
        return fs.readdir(filePath, (err, files) => {
            if(err){
                console.log('要查看的文件路径错误');
                return reject(true);
            };
            console.log(files);
            let writeFileName = files.filter(function(currentValue, index, arr){
                return searchName.test(currentValue);
            })[0];
            if (!writeFileName) {
                console.log('没找到文件');
                return reject(true);
            };
            console.log(writeFileName);
            return resolve(false);
        });
    });
};

