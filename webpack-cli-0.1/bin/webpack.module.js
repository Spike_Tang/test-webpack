const path = require('path');

module.exports = (data) => {
    const {
        buildFilePath
    } = data;

    return {
        rules: [{
            test: /\.(js|jsx)$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
                plugins: ['transform-runtime', ["import", { libraryName: "antd-mobile", style: "css" }]],
                presets: ['es2015', 'react', 'stage-2']
            }
        },{
            test: /\.css$/,
            use: [{
                    loader: 'style-loader'
                },{
                    loader: 'css-loader'
                },{
                    loader: 'postcss-loader'
            }]
        },{
            test: /\.scss$/,
            use: [{
                loader: 'style-loader'
            },{
                loader: 'css-loader'
            },{
                loader: 'postcss-loader'
            },{
                loader: 'sass-loader'
            }]
        },{
            test: /\.less$/,
            use: [{
                loader: 'style-loader'
            },{
                loader: 'css-loader',
            },{
                loader: 'less-loader',
            }]
        },{
            test: /\.(png|svg|jpg|gif|jpeg)$/,
            use: [{
                loader: 'url-loader',
                options: {
                    limit: 2048
                }
            }]
        },{
            test: /\.(woff|woff2|eot|ttf|otf|mp4)$/,
            use: [{
                loader: 'url-loader',
                options: {}
            }]
        },{
            test: /\.(ejs)$/,
            use: ['ejs-loader']
        }]
    };
};

