const path = require('path');
const fs = require('fs');

const express = require('express');
const myApp = express();
const readlineSync = require('readline-sync');

const webpack = require('webpack');
const merge = require('webpack-merge');
const webpackDevServer = require('webpack-dev-server');
const DllReferencePlugin = require('webpack/lib/DllReferencePlugin');
const colors = require('colors');

// 获取 && 验证 运行参数
/*
    例如：有一个命令 "main": "node ./bin/main.js"
    运行 npm run main dev /activity/project
    实际会转化成 node ./bin/main.js "dev" "/activity/20180513-open-box" 这个在控制台第一行也会返回
    这时 process.argv 里面的数据有4个
    [
        '/usr/local/bin/node',
        '/Users/luoqing/Desktop/myData/test-webpack/webpack-cli-0.1/bin/main.js',
        'dev',
        '/activity/20180513-open-box'
    ]
    第一个对应的是 node 的路径
    第二个文件的绝对路径
    第三 四个 参数的字符串
*/
run();

function run () {
    const environmentsName = ['dev', 'watch', 'test', 'build'];
    const argvs = process.argv;
    let environmentState = false;
    let projectPathState = false;

    let environment = '';
    let projectPath = '';

    if (argvs.length === 4) {
        environmentState = isEnvironment(argvs[2]);
        projectPathState = isProjectPath(argvs[3]);
        if (environmentState) {
            environment = argvs[2];
        };
        if (projectPathState) {
            projectPath = argvs[3];
        };
    } else if (argvs.length === 3) {
        environmentState = isEnvironment(argvs[2]);
        projectPathState = isProjectPath(argvs[2]);

        if (environmentState) {
            environment = argvs[2];
        };
        if (projectPathState) {
            projectPath = argvs[2];
        };
    };
    if (!environmentState) {
        // 缺少环境参数
        console.log('Please select environment!'.green);
        environmentsName.forEach((item, index) => {
            console.log(`[${index}] ${item}`.green);
        });
        let environmentIndex = readlineSync.question('Which one do you want to choose?'.green);
        console.log(environmentsName[environmentIndex].green);
        environment = environmentsName[environmentIndex];
    };
    if (!projectPathState) {
        // 缺少路径参数
        // 加载项目路径配置文件
        let projects = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../src/project-path.json')));
        console.log('Please select project!'.green);
        let projectKeys = Object.keys(projects);
        projectKeys.forEach((item, index) => {
            console.log(`[${index}] ${item}`.green);
        });
        let projectIndex = readlineSync.question('Which one do you want to choose?'.green);
        console.log(projectKeys[projectIndex].green);
        projectPath = path.resolve(__dirname, `../src${projects[projectKeys[projectIndex]]}`);
    };

    const defaultConfig = require('./base.config.js');
    const projectConfig = require(`${projectPath}/config.js`);
    const buildPath = path.join(defaultConfig.rootBuildPath, projectConfig.buildFilePath);
    // 合并项目配置 和 默认配置
    let config = merge(defaultConfig, projectConfig);
    config.buildPath = buildPath;
    config.environment = environment;
    config.projectPath = projectPath;

    (async function() {
        console.log('开始运行')
        if ( config.hasDllReferencePlugin) {
            let isVendor = await searchFilePathByName(buildPath, /^(vendor).*(js)$/).catch((bool) => bool);
            if (isVendor) {
                await runWebpack(require('./webpack.dll.js'));
                console.log('vender准备就绪');
            };
        };
        // 传入配置想 组合生成 webpack.config
        const webpackConfig = {
            entry: {
                index: path.join(projectPath, config.entryJsName)
            },
            output: require('./webpack.output.js')(config),
            module: require('./webpack.module.js')(config),
            plugins: require('./webpack.plugin.js')(config),
            devtool: require('./webpack.server.js')(config).devtool,
            resolve: require('./webpack.resolve.js')(config)
        };
        const devOptions = require('./webpack.server.js')(config).devServer;
        console.log('开始判断环境');
        if (environment === 'dev') {
            console.log('本地开发环境');
            // webpackDevServer.addDevServerEntrypoints 需要在 webpack(webpackConfig) 之前
            webpackDevServer.addDevServerEntrypoints(webpackConfig, devOptions);
            const compiler = webpack(webpackConfig, (e) => {
                console.log(new Date(), e);
            });
            // 修改为默认文件系统，即将文件生成到目录中而不是内存中
            // compiler.outputFileSystem = fs;
            const server = new webpackDevServer(compiler, devOptions);
            server.listen(devOptions.port, devOptions.host, (err) => {
                console.log(`dev server listening on port ${devOptions.port}`);
            });
        }else if (environment === 'watch') {
            console.log('连调环境');
            console.log(config.watchOption);
            let option = config.watchOption;
            let publicPath = option.publicPath;
            // 如果存在需要指定输入名称
            if (option.filename) {
                webpackConfig.output.filename = option.filename;
                // console.log(option.filename);
                // console.log(webpackConfig.output.filename);
                // webpackConfig.output.filename = '[name].js';
            };
            // 启动webpack watch 模式
            const compiler = webpack(webpackConfig);
            compiler.watch({
                aggregateTimeout: 300,
                poll: undefined
            }, (err, stats) => {
                console.log(`重新打包完成 ${new Date()}`.green);
            });
            // 启动express 静态文件服务
            myApp.use(publicPath, express.static(config.buildPath));
            let server = myApp.listen(devOptions.port, () => {
                console.log(`dev server listening on port ${devOptions.port}`.green);
            });
        }else {
            console.log('生产环境或者测试环境');
            await runWebpack(webpackConfig);
        };
        console.log('运行结束');
    })();

    function isEnvironment (str) {
        return environmentsName.some((item, index, array) => {
            return item === str;
        });
    };

    function isProjectPath (str) {
        try{
            let projectConfig = require(projectConfigPath(str));
            return true;
        } catch (e) {
            console.log('项目路径错误，或者项目根目录中没config.js'.red);
            console.log(`路径：${projectConfigPath(str)}`.red);
            return false;
        };
    };

    function projectConfigPath (configPath) {
        return path.resolve(__dirname, `../src${configPath}/config.js`);
    };

    function runWebpack(config) {
        return new Promise((resovle, reject) => {
            return webpack(config).run((err, state) => {
                if (err) {
                    return reject(err);
                };
                console.log(state.toString({
                    // all: 'normal',
                    chunks: true,
                    chunkModules: true,
                    chunkOrigins: true,
                    colors: true    // 在控制台展示颜色
                }));
                return resovle(state);
            });
        });
    };

    function searchFilePathByName (filePath, searchName) {
        return new Promise((resolve, reject) => {
            return fs.readdir(filePath, (err, files) => {
                if(err){
                    console.log('要查看的文件路径错误');
                    return reject(true);
                };
                console.log(files);
                let writeFileName = files.filter(function(currentValue, index, arr){
                    return searchName.test(currentValue);
                })[0];
                if (!writeFileName) {
                    console.log('没找到文件');
                    return reject(true);
                };
                console.log(writeFileName);
                return resolve(false);
            });
        });
    };
};

// const npmArgument = JSON.parse(process.env.npm_config_argv).original;
// const npmArgumentLength = npmArgument.length - 1;
// const environment = npmArgument[npmArgumentLength - 1].replace(/--/, '');

// const projectPath = path.resolve(__dirname, `../src${npmArgument[npmArgumentLength].replace(/--/, '')}`);
// const projectConfigPath = `${projectPath}/config.js`;
// const defaultConfig = require('./base.config.js');
// const projectConfig = require(projectConfigPath);

// const buildPath = path.join(defaultConfig.rootBuildPath, projectConfig.buildFilePath);
// // 合并项目配置 和 默认配置
// let config = merge(defaultConfig, projectConfig);
// config.buildPath = buildPath;
// config.environment = environment;
// config.projectPath = projectPath;

// let testCdn = '/c2p/app_store/mtlive/activity/20180513-open-box';

// // 使用webpack node API 启动 vendor 打包
// (async function() {
//     console.log('开始运行')
//     let isVendor = await searchFilePathByName(buildPath, /^(vendor).*(js)$/).catch((bool) => bool);
//     if ( config.hasDllReferencePlugin && isVendor) {
//         await runWebpack(require('./webpack.dll.js'));
//         console.log('vender准备就绪');
//     };
//     // 传入配置想 组合生成 webpack.config
//     const webpackConfig = {
//         entry: {
//             index: path.join(projectPath, config.entryJsName)
//         },
//         output: require('./webpack.output.js')(config),
//         module: require('./webpack.module.js')(config),
//         plugins: require('./webpack.plugin.js')(config),
//         devtool: require('./webpack.server.js')(config).devtool,
//         resolve: require('./webpack.resolve.js')(config)
//     };
//     const devOptions = require('./webpack.server.js')(config).devServer;
//     console.log('开始判断环境');
//     if (environment === 'dev') {
//         console.log('本地开发环境');
//         // webpackDevServer.addDevServerEntrypoints 需要在 webpack(webpackConfig) 之前
//         webpackDevServer.addDevServerEntrypoints(webpackConfig, devOptions);
//         const compiler = webpack(webpackConfig, (e) => {
//             console.log(new Date(), e);
//         });
//         // 修改为默认文件系统，即将文件生成到目录中而不是内存中
//         // compiler.outputFileSystem = fs;
//         const server = new webpackDevServer(compiler, devOptions);
//         server.listen(devOptions.port, devOptions.host, (err) => {
//             console.log(`dev server listening on port ${devOptions.port}`);
//         });
//     }else if (environment === 'watch') {
//         console.log('连调环境');

//         const compiler = webpack(webpackConfig);
//         compiler.watch({
//             aggregateTimeout: 300,
//             poll: undefined
//         }, (err, stats) => {
//             console.log(`重新打包完成 ${new Date()}`);
//         });am

//         myApp.use(testCdn, express.static(config.buildPath));
//         let server = myApp.listen(devOptions.port, () => {
//             console.log(`dev server listening on port ${devOptions.port}`);
//         });
//     }else {
//         console.log('生产环境或者测试环境');
//         await runWebpack(webpackConfig);
//     };
//     console.log('运行结束');
// })();

// function runWebpack(config) {
//     return new Promise((resovle, reject) => {
//         return webpack(config).run((err, state) => {
//             if (err) {
//                 return reject(err);
//             };
//             console.log(state.toString({
//                 // all: 'normal',
//                 chunks: true,
//                 chunkModules: true,
//                 chunkOrigins: true,
//                 colors: true    // 在控制台展示颜色
//             }));
//             return resovle(state);
//         });
//     });
// };

// function searchFilePathByName (filePath, searchName) {
//     return new Promise((resolve, reject) => {
//         return fs.readdir(filePath, (err, files) => {
//             if(err){
//                 console.log('要查看的文件路径错误');
//                 return reject(true);
//             };
//             console.log(files);
//             let writeFileName = files.filter(function(currentValue, index, arr){
//                 return searchName.test(currentValue);
//             })[0];
//             if (!writeFileName) {
//                 console.log('没找到文件');
//                 return reject(true);
//             };
//             console.log(writeFileName);
//             return resolve(false);
//         });
//     });
// };

