module.exports = (data) => {
    const {
        buildPath,
        test_publicPath,
        pro_publicPath,
        test_outputNameHasHash,
        pro_outputNameHasHash,
        environment
    } = data;

    let name = '';

    if( environment === 'build' ){
        name = pro_outputNameHasHash? '[name].[hash].js' :'[name].js';
    }else if ( environment === 'test' ) {
        name = test_outputNameHasHash? '[name].test.[hash].js' :'[name].test.js';
    }else{
        name = '[name].dev.js';
    };

    return {
        path: buildPath,
        filename: name,
        chunkFilename: name,
        publicPath: environment === 'build' ? pro_publicPath : environment === 'test' ? test_publicPath : ''
    };
};

