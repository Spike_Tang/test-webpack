import React from 'react';
import ReactDOM from 'react-dom';
// import Routes from './routes/routes.js';

import './index.css';

class GoTop extends React.Component {
    constructor (props) {
        super(props);
    };

    render () {
        return (
            <div>test</div>
        )
    };
};

setTimeout(() => {
    try{
        MTJs.onSharePageInfo({
            title: '分享标题',
            image: '分享图片',
            description: '分享描述',
            link: '分享的地址',
            success:function(e) {
                // code
                alert('分享回调');
                alert(e.data.type);
            }
        });
    }catch(e){
        alert(e)
    };
}, 1000)

window.addEventListener("_postMessage_", (e) => {
    alert('监听_postMessage_');
    alert(e);
});
// window.addEventListener("message", (e) => {
//     alert('监听message');
//     alert(e);
// });


ReactDOM.render(
    <GoTop />,
	document.getElementById('root')
);

// 热替换HMR，需要加入这段代码才会进行生效
if(module.hot){
    module.hot.accept();
};