import React from 'react';

import { Link, hashHistory } from 'react-router';
import Swiper from 'swiper';

class MusicStation extends React.Component {
    constructor (props) {
        super(props);

        this.musicListData = props.dataList || [];
        this.hasMTnative = sessionStorage.getItem('isMTnative') == 'true'? true: false;
    };

    shouldComponentUpdate (nextProps, nextState) {
        if ( this.musicListData !== nextProps.dataList ) {

            this.musicListData = nextProps.dataList;
            
            return true;
        };
        return false;
    };

    musicListTemplate (data) {
        if ( !data.map ) return '';
        return data.map((item, index, arr) => {
            return (
                <a className="item swiper-slide" key={index}
                    href={this.hasMTnative?
                            `mtmv://media?id=${item.media_id}`:
                            `https://www.meipai.com/media/${item.media_id}`
                        }>
                    <div className="img global-img-default">
                        <div className="global-img" style={{backgroundImage: `url(${item.cover_img})`}}></div>
                    </div>
                    <p className="name"><span>{item.name}</span></p>
                </a>
            );
        });
    };

    render () {
        return (
            <div className="music-station">
                <div className="music-list-wrap">
                    <div className="list swiper-wrapper">
                        {this.musicListTemplate(this.musicListData)}
                    </div>
                </div>
                <div className="btn-left" onClick={() => this.slidePrev()}></div>
                <div className="btn-right" onClick={() => this.slideNext()}></div>
            </div>
        )
    };

    componentDidMount () {
        this.swiperDOM = this.swiperInit(); 
    };

    componentDidUpdate () {
        this.swiperDOM.destroy(false);
        this.swiperDOM = this.swiperInit(); 
    };

    componentWillUnmount () {
        this.swiperDOM.destroy(false);
    };

    swiperInit () {
        return new Swiper('.music-list-wrap',{
            slidesPerView : 3,
            centeredSlides : false,
        });
    };

    slidePrev () {
        this.swiperDOM.slidePrev();
    };

    slideNext () {
        this.swiperDOM.slideNext();
    };
};

export default MusicStation;