import React from 'react';

import { Link, hashHistory } from 'react-router';
import Swiper from 'swiper';

import vImg from 'imgPath/v.png';
class LastWeek extends React.Component {
    constructor (props) {
        super(props);

        this.starListData = props.dataList || [];
        this.hasMTnative = sessionStorage.getItem('isMTnative') == 'true'? true: false;
    };

    shouldComponentUpdate (nextProps, nextState) {
        if ( this.starListData !== nextProps.dataList ) {

            this.starListData = nextProps.dataList;
            
            return true;
        };
        return false;
    };

    starListTemplate (data) {
        if ( !data.map ) return '';
        return data.map((item, index, arr) => {
            return (
                <li className="item swiper-slide" key={index}>
                    <div className="icon global-img-default">
                        <div className="global-img" style={{backgroundImage: `url(${item.avatar})`}}></div>
                    </div>
                    <img src={vImg} className={`item-v ${item.verified == 1? 'active': ''}`}/>
                    <p className="name">{item.nickname}</p>
                    <a className="go-star-page" 
                        href={this.hasMTnative?
                            `mtmv://user?id=${item.uid}`:
                            `https://www.meipai.com/user/${item.uid}`
                        }>
                    </a>
                    <a className="go-video-page"
                        href={this.hasMTnative?
                            `mtmv://media?id=${item.media_id}`:
                            `https://www.meipai.com/media/${item.media_id}`
                        }>
                    </a>
                </li>
            );
        });
    };

    render () {
        return (
            <div className="last-week">
                <div className="star-list-wrap">
                    <ul className="list swiper-wrapper">
                        {this.starListTemplate(this.starListData)}
                    </ul>
                </div>
                <div className="btn-left" onClick={() => this.slidePrev()}></div>
                <div className="btn-right" onClick={() => this.slideNext()}></div>
            </div>
        )
    };

    componentDidMount () {
        this.swiperDOM = this.swiperInit(); 
    };

    componentDidUpdate () {
        this.swiperDOM.destroy(false);
        this.swiperDOM = this.swiperInit(); 
    };

    componentWillUnmount () {
        this.swiperDOM.destroy(false);
    };

    swiperInit () {
        return new Swiper('.star-list-wrap',{
            slidesPerView : 3,
            centeredSlides : false,
        });
    };

    slidePrev () {
        this.swiperDOM.slidePrev();
    };

    slideNext () {
        this.swiperDOM.slideNext();
    };
};

export default LastWeek;