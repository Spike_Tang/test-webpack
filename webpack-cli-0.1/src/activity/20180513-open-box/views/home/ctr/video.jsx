import React from 'react';

import loadingIcon from 'imgPath/loading.png';

export default class Video extends React.Component {
/*
                <video
                    webkit-playsinline="true"
                    controls='controls'
                    className={`iphone video`} ></video>

                <div className={`${this.state.isShowVideoWrap?'active':''} video-back`}>
                    <a onClick={() => this.videoEnd()}
                        className={`${this.state.isShowVideoWrap?'active':''} goback`}>X</a>
                    <video className={`qq video`}
                        controls="controls"
                        playsinline
                        x5-video-player-fullscreen="true">
                    </video>
                </div>

                <div className={`${this.state.isShowVideoWrap?'active':''} video-back`}>
                    <a onClick={() => this.videoEnd()}
                        className={`${this.state.isShowVideoWrap?'active':''} goback`}>X</a>
                    <img src={loading} className={`loading-icon ${this.state.isVisibleVideo?'hide':'show'}`}/>
                    <video
                        controls="controls"
                        webkit-playsinline
                        className={`anzhuo video ${this.state.isVisibleVideo?'show':''}`}></video>
                </div>
*/
    constructor (props) {
        super(props);

        this.state = {
            // video 弹层 显示隐藏
            isShowVideoWrap: false,
            // 安卓 播放时video 显示隐藏
            isVisibleVideo: false,
            // iphone 播放时load 显示隐藏
            isLoadingIcon: false
        };

        this.videoDom = null;
        this.videoTemplate = this.setVideoTemplate();
    };

    setVideoTemplate () {
        if (/iPhone/.test(navigator.userAgent)) {
            return (
            	<div>iphone</div>
            );
        }else if (/QQ/.test(navigator.userAgent)) {
            return (
            	<div>android QQ</div>
            );
        }else{
            return (
                <div className={`${this.state.isShowVideoWrap?'active':''} video-back`}>
                    <a onClick={() => this.videoEnd()}
                        className={`${this.state.isShowVideoWrap?'active':''} goback`}>X</a>
                    <img src={loadingIcon} className={`loading-icon ${this.state.isVisibleVideo?'hide':'show'}`}/>
                    <video
                        controls="controls"
                        webkit-playsinline
                        className={`anzhuo video ${this.state.isVisibleVideo?'show':''}`}></video>
                </div>
            );
        };
    };

    componentWillMount (nextProps, nextState) {
    	console.log('componentWillMount')
    };

	render () {
		return this.videoTemplate;
	};

    componentDidMount () {
        console.log('componentDidMount')
        this.videoDom = this.initVideo();
        this.videoPlay(null, 'https://public2.video.meipai.com/1c4d00c5f441ceadb788689cb0facf29.mp4');
    };

    /* -----------------------------video start----------------------------------------- */
    initVideo () {
        let dom = document.getElementsByClassName('video')[0];

        dom.addEventListener('ended', () => {
            this.videoEnd();
        });
        dom.addEventListener('canplay', () => {
            this.setState({
                isVisibleVideo: true,
                isLoadingIcon: false
            }, () => {

            });
        });

        return dom;
    };

    videoPlay (e, url) {
        if (!url) {
            return;
        };
        this.setState({
            isVisibleVideo: false,
            isLoadingIcon: true,
            isShowVideoWrap: true
        },() => {

            this.videoDom.src = url;
            this.videoDom.volume = 0;
            this.videoDom.play();
        });
    };

    videoEnd () {
        this.setState({
            isVisibleVideo: false,
            // isLoadingIcon 在播放时才让他显示，播放结束让他显示在iphone 下有bug
            isLoadingIcon: false,
            isShowVideoWrap: false
        }, () => {

            this.videoDom.pause();
            this.videoDom.currentTime = 0;
        });
    };
}