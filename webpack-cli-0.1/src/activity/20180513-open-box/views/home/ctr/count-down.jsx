import React from 'react';
import {addZeroToStartPosition} from 'utilsPath/string-function.js';
// 倒计时
// endTime   number      距离结束剩余的时间   初始化时传 false
// callback  function    结束后的回调
export default class CountDowmComponent extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            endDays: '00',
            endhours: '00',
            endminutes: '00',
            endSecond: '00',
            test: false
        };

        this.timer = null;
        this.seconds = 1000;
        this.minutes = this.seconds * 60;
        this.hours = this.minutes * 60;
        this.days = this.hours * 24;

        this.oldProps = props;
    };

    componentWillMount () {
        // 获取结束时间和 客户端服务端时间差
        this.startCountDown(this.props.endTime, this.props.callback);
    };
    shouldComponentUpdate(nextProps, nextState) {
        // 拦截无效的render
        let isChangeProps = JSON.stringify(this.oldProps) !== JSON.stringify(nextProps);
        let isChangeState = JSON.stringify(this.state) !== JSON.stringify(nextState);
        if (isChangeProps) {
            this.oldProps = nextProps;
            this.clearCountDown();
            this.startCountDown(nextProps.endTime, nextProps.callback);
            return true;
        }else if(isChangeState) {
            return true;
        };
        return false;
    };

    componentWillUnmount () {
        this.clearCountDown();
    };

    render () {
        console.log('render child');
        return (     
            <div className="count-down">
                <p className="text">
                    <span className="num">{this.state.endDays}</span><span className="unit">天</span><span className="num">{this.state.endhours}</span><span className="unit">时</span><span className="num">{this.state.endminutes}</span><span className="unit">分</span><span className="num">{this.state.endSecond}</span><span className="unit">秒</span>
                </p>
            </div>
        );
    };
    
    /* ----------------------------倒计时------------------------------------------ */
    startCountDown (endTime, callback) {
        if ( this.endTime === false ) return;
        if ( this.timer ) return;
        // this.endTime 初始值为 false 就是不启动倒计时。 0就是启动倒计并且直接到达结束时间
        let startTime = new Date().getTime();

        const count = (startTime, endTime, callback) => {
            let remaining = startTime + endTime - new Date().getTime();
            if ( remaining <= 0 ) {
                this.setState({
                    endDay: '00',
                    endhours: '00',
                    endminutes: '00',
                    endSecond: '00'
                });
                clearInterval(this.timer);
                this.timer = null;

                try{
                    callback();
                }catch (e) {

                }; 
                return;
            }else if( remaining >= this.days * 100) {
                this.setState({
                    endDay: 99,
                    endhours: 99,
                    endminutes: 59,
                    endSecond: 59
                });
                return;
            };
            let days = Math.floor(remaining / this.days );
            let hours = Math.floor((remaining - (days * this.days)) / this.hours );
            let minutes = Math.floor((remaining - (days * this.days) - (hours * this.hours)) / this.minutes );
            let seconds = Math.floor((remaining - (days * this.days) - (hours * this.hours) - (minutes * this.minutes)) / this.seconds );

            this.setState({
                endDays: addZeroToStartPosition(days),
                endhours: addZeroToStartPosition(hours),
                endminutes: addZeroToStartPosition(minutes),
                endSecond: addZeroToStartPosition(seconds)
            });
        };

        count(startTime, endTime, callback);
        this.timer = setInterval(() => {
            count(startTime, endTime, callback);
        }, 250);
    };

    clearCountDown () {
        clearInterval(this.timer);
        this.timer = null;
    };
};

