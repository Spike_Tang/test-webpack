import HomePage from './index.jsx';
import Rules from '../rules/index.jsx';

export const IndexHome = (store) => ({
    component: HomePage,
    childRoutes: [
        // {
        //     path: '/rules',
        //     component: Rules,
        // }
    ],
    onEnter: (params, replace) => {
    	// 验证
    	if (window.globalData.agent.isWeb) {
    		replace('/hint');
    	};
    },
    onLeave: (params, replace) => {

    }
});

export const Home = (store) => ({
    path: '/home',
    component: HomePage,
    childRoutes: [
        // {
        //     path: '/rules',
        //     component: Rules,
        // }
    ],
    onEnter: (params, replace) => {
    	// 验证
    	if (window.globalData.agent.isWeb) {
    		replace('/hint');
    	};
    },
    onLeave: (params, replace) => {

    }
});
