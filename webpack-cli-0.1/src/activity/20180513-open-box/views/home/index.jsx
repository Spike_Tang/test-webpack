import './index.scss';

import React from 'react';
import { Link, hashHistory } from 'react-router';
import { ajax } from 'utilsPath/fetch.js';
import Swiper from 'assetsPath/swiper-4.2.2.min.js';
import 'whatwg-fetch';
import { sliceStringAccordingToByte } from 'utilsPath/string-function.js';

import imgDefault from 'imgPath/img-default.jpg';
import iconDefault from 'imgPath/icon-default.png';
import loadingIcon from 'imgPath/loading.png';
import listDefaultImg from 'imgPath/list-default.png';
import applyButtonImg from 'imgPath/apply-button.png';
import starListItemBtnIcon from 'imgPath/btn.png';
import starListGoBackIcon from 'imgPath/list-goback.png';
import dotIcon from 'imgPath/dot.png';
import equipmentState1 from 'imgPath/equipment-state1.png';
import equipmentState2 from 'imgPath/equipment-state2.png';
import equipmentState3 from 'imgPath/equipment-state3.png';
import combatBtnIcon from 'imgPath/combat-btn.png';
import finalHint1 from 'imgPath/final-hint1.png';
import finalHint2 from 'imgPath/final-hint2.png';
import finalBtn from 'imgPath/final-btn.png';
import searchBtn from 'imgPath/btn-search-btn.png';
import useBtn from 'imgPath/btn-use-btn.png';
import equipmentBackIntroOnly from 'imgPath/equipment-back-intro_02_only.png';
import equipmentBackIntro from 'imgPath/equipment-back-intro_02.png';

import CountDownComponent from './ctr/count-down.jsx';

import {StaticImage} from 'componentPath/imgs.jsx';

let HeaderComponent = (props) => {
    return (
        <div className="header"></div>
    );
};

let RulesComponent = () => {
    return (
        <div className="rules">
            <Link className="click-drive" to="/rules"></Link>
        </div>
    );
};

let MyDataComponent = (props, login) => {
    let {
        isLogin,
        isApply,
        rank,
        avatar,
        screenName,
        score,
        todayScore,
        incrScore,
        equipmentIng,
        redirectFn,
        uid
    } = props;
    let loginFn = props.login;
    let userEquipment = '';
    if ( !isLogin ) {
        // 未登陆
        userEquipment = <img src={applyButtonImg} className="button" onClick={() => loginFn()}/>;
    }else {
        // 登陆
        if ( isApply ) {
            // 登陆已报名
            if ( Array.isArray(equipmentIng) && equipmentIng.length > 0) {
                // 有使用中的装备
                userEquipment = equipmentIng.map((item, index, array) => {
                    return <div className="item" key={index}>{`${item.rate}%`}</div>
                });
            }else{
                userEquipment = <p className="no-data">暂无生效装备</p>;
            };
        } else {
            // 登陆未报名
            userEquipment = <img src={applyButtonImg} className="button" onClick={() => loginFn()}/>;
        };
    };

    return (
        <div className="my-data">
            {
                !isNaN(Number(score)) && Number(score) != 0
                    ?<p className="rank">{`排名 ${rank}`}</p>
                    :''
            }
            <div className={`head-img global-img-default`} onClick={() => redirectFn('user', uid)}>
                <div className="global-img" style={{backgroundImage: `url(${avatar})`}}></div>
            </div>
            <p className="nickname">{screenName}</p>
            <div className="equipment-data">
                {
                    userEquipment
                }
            </div>
            <p className="text">{`装备值：${score}`}</p>
            <p className="doc"><span className="item margin-r">{`翻倍装备值：${incrScore}`}</span><span className="item">{`今日装备值：${todayScore}`}</span></p>
        </div>
    );
};

let StarListComponent = (props) => {
    let {
        list,
        robChance,
        redirect,
        changeFn,
        searchFn,
        searchGoBackFn,
        finalSelectFn,
        searchId,
        isSearch,
        uid,
    } = props;
    let items = [];
    try{
        items = list.map((item, index, arr) => {
            let equipments = [];
            let styleName = '';
            try{
                equipments = item.equipmentIng.map((itemChild, indexChild, arrChild) => {
                    return (
                        <div className="equipment-item" key={indexChild}>{`${itemChild}%`}</div>
                    )
                });
            }catch(e){
                equipments = [];
            };
            switch(index){
                case 0:
                    styleName= 'left one';
                break;
                case 1:
                    styleName= 'left two';
                break;
                case 2:
                    styleName= 'left three';
                break;
                default:
                    styleName= 'left';
                break;
            };
            // 点击传入类型。 是直播间还是个人页
            let redirectType = item.isLive?'live' :'user';
            // 点击传入的对应id
            let redirectId = item.isLive?item.liveId :item.uid;
            // 中间元素样式判断

            // 当自己也在列表中，添加一个特殊的class
            let itemClassName = '';
            if (item.uid == uid) {
                // 自己的item
                itemClassName = 'is-my';
            };
            if (equipments.length == 0) {
                // 没有装备
                return (
                    <div className={`item no-eq ${itemClassName}`} key={index}>
                        <div className={styleName}
                        onClick={() => redirect(redirectType, redirectId)}>
                            <div className="key">{item.rank}</div>
                            <StaticImage className="head-img" src={item.avatar}/>
                            <p className="name">{sliceStringAccordingToByte(item.screenName, 12)}</p>
                            <div className={`mark ${item.isLive? 'active': ''}`}></div>
                        </div>
                        <div className="middle">
                            <p className="combat no-eq">
                                {`装备值：${item.score}`}
                            </p>
                        </div>
                        <div className="right">
                            {
                                robChance == 0
                                    ?''
                                    :robChance == index + 1
                                        ?<img src={starListItemBtnIcon} className="btn" onClick={(e) => finalSelectFn(e,item.uid)}/>
                                        :''
                            }
                        </div>
                    </div>
                );
            }else{
                // 有装备
                return (
                    <div className={`item ${itemClassName}`} key={index}>
                        <div className={styleName}
                        onClick={() => redirect(redirectType, redirectId)}>
                            <div className="key">{item.rank}</div>
                            <StaticImage className="head-img" src={item.avatar}/>
                            <p className="name">{sliceStringAccordingToByte(item.screenName, 12)}</p>
                            <div className={`mark ${item.isLive? 'active': ''}`}></div>
                        </div>
                        <div className="middle">
                            <p className="combat">
                                {`装备值：${item.score}`}
                            </p>
                            <div className="equipment">
                                {equipments}
                            </div>
                        </div>
                        <div className="right">
                            {
                                robChance == 0
                                    ?''
                                    :robChance == index + 1
                                        ?<img src={starListItemBtnIcon} className="btn" onClick={(e) => finalSelectFn(e,item.uid)}/>
                                        :''
                            }
                        </div>
                    </div>
                );
            };
        });
        // 正常状态下 不足20条需要补齐， 搜索状态下不需要
        if ( items.length < 20 && !isSearch) {
            for (let i=0; i<20-items.length; i++) {
                items.push(<div className="item" key={i+100} style={{backgroundImage: `url('${listDefaultImg}')`}}></div>);
            };
        };
    } catch(e) {
        items = [];
    };
    return (
        <div className="star-list">
            <div className="list-head">
            </div>
            <div className="list-repeat">
                {
                    items.length >= 100 || isSearch
                        ?
                            <div className="list-search">
                                <input type="text" className="search"
                                    placeholder="输入美拍ID可搜索"
                                    onChange={(e) => changeFn(e)}/>
                                <img className="img" src={searchBtn} onClick={() => searchFn()}/>
                            </div>
                        :''
                }
                {items}
                {
                    isSearch
                        ?<img src={starListGoBackIcon} className="goback" onClick={() => searchGoBackFn()}/>
                        :''
                }
            </div>
            <div className="list-bottom">
                {
                    isSearch
                        ?''
                        :list.length >= 100
                            ?'此榜单只显示前100名，支持搜索更多主播'
                            :'没有更多了...'
                }
            </div>
        </div>
    );
};

class GoTop extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isShowTopBtn: false,
        };
        this.working = true;
        this.htmlDom = null;
        this.countDownDom = null;
    };
    componentDidMount() {
        // this.html = document.getElementsByTagName('html')[0];
        this.countDownTop = document.getElementsByClassName('count-down')[0];
        this.starListTop = document.getElementsByClassName('star-list')[0];
        setTimeout(() => {
            window.addEventListener('scroll', this.watchScroll.bind(this));
            this.working = false;
        }, 100);
    };
    render () {
        return (
            <div className={`go-top ${this.state.isShowTopBtn? 'active': ''}`}
                onClick={() => this.clickGoToTop()}></div>
        )
    };
    watchScroll () {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
        if (this.working) return;
        // console.log(456)
        // 降低执行频率
        this.working = true;
        if ( scrollTop > this.starListTop.offsetTop) {
            // 拦截重复赋值
            if (this.state.isShowTopBtn){
                this.working = false;
                return;
            };
            this.setState({
                isShowTopBtn: true
            }, () => this.working = false);
        }else{
            // 拦截重复赋值
            if (!this.state.isShowTopBtn){
                this.working = false;
                return;
            };
            this.setState({
                isShowTopBtn: false
            }, () => this.working = false);
        };
    };
    clickGoToTop () {
        $("body,html").animate({scrollTop: this.countDownTop.offsetTop}, 500);
        // this.html.scrollTop = this.countDownTop.offsetTop;
    };
    componentWillUnmount() {
        // 防止初始化就运行 watchScroll 然后报错；
        this.working = true;
    };
};

class EquipmentAcquire extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            list: props.list || []
        };
        this.oldProps = props;
        this.swiperDOM = null;
    };
    render () {
        let items = [];
        let list = this.state.list;
        list = list.sort((itemA, itemB) => {
            let a = 0;
            let b = 0;
            a = a + Number(!itemA.ing) + Number(itemA.effective);
            b = b + Number(!itemB.ing) + Number(itemB.effective);
            return b - a;
        });

        items = list.map((item, index, array) => {
            // effective true 有效期内 false 过期
            // ing true 使用中 false 未使用
            return (
                <li className="item" key={index}>
                    <div className="intro">
                        <p className="title">{`+${item.rate}%`}</p>
                        <p className="text">积分加成</p>
                        {
                            !item.effective || item.ing
                                ?<div className="shadow"></div>
                                :''
                        }
                    </div>
                    <p className="hint-title">
                        {
                            !item.effective? '已过期'
                                : item.ing? '使用中': '可使用'
                        }
                    </p>
                    {
                        item.effective && !item.ing && item.expire_at < 1531152000
                            ? <p className="hint-text">今晚24点过期</p>
                            : ''
                    }
                </li>
            );
        });
        return (
            <div className="acquire"
                style={{backgroundImage: `url('${list.length <=3? equipmentBackIntroOnly: equipmentBackIntro}')`}}>
                {
                    this.state.list.length > 3
                        ?<div className="left-btn" onClick={() => this.slidePrev()}></div>
                        :''
                }
                {
                    this.state.list.length > 3
                        ?<div className="right-btn" onClick={() => this.slideNext()}></div>
                        :''
                }
                {
                    this.state.list.length > 0
                        ?
                            <div className={`acquire-list-wrap`}>
                                <ul className="list">
                                    {items}
                                </ul>
                            </div>
                        :
                            <div className="default active">
                                <p className="hint-title">暂无</p>
                            </div>
                }
            </div>
        );
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            list: nextProps.list
        });
    };

    // shouldComponentUpdate(nextProps, nextState) {
    //     // 拦截无效的render
    //     let isChangeProps = JSON.stringify(this.oldProps) !== JSON.stringify(nextProps);
    //     let isChangeState = JSON.stringify(this.state) !== JSON.stringify(nextState);
    //     if (isChangeProps) {
    //         this.oldProps = nextProps;
    //         return true;
    //     }else if(isChangeState) {
    //         return true;
    //     };
    //     return false;
    // };

    componentDidMount () {
        this.swiperDOM = this.swiperInit();
    };

    componentDidUpdate () {
        try{
            this.swiperDOM.destroy(false);
        }catch(e){

        };
        // list 不足3个的时候启动 swiper 会导致间距增加
        if (this.state.list.length >= 3) {
            this.swiperDOM = this.swiperInit();
        };
    };

    swiperInit () {
        return new Swiper('.acquire-list-wrap',{
            slidesPerView : 3,
            centeredSlides : false,
            wrapperClass: 'list',
            slideClass: 'item'
        });
    };

    slidePrev () {
        this.swiperDOM.slidePrev();
    };

    slideNext () {
        this.swiperDOM.slideNext();
    };
};

class FinalHint extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isShow: props.isShow,
            closeFn: props.closeFn
        };
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            isShow: nextProps.isShow,
            closeFn: nextProps.closeFn
        });
    };

    render () {
        return (
            <div className={`final-hint ${this.state.isShow? 'active': ''}`}>
                <div className="final-list-wrap">
                    <ul className="list">
                        <li className="item">
                            <img className="img" src={finalHint1}/>
                            <p className="hint-text">即刻起，榜单中名次上升，</p>
                            <p className="hint-text">即可抢夺<span className="color">下一名次</span>的装备！</p>
                            <p className="hint-text">*限榜单前99名</p>
                        </li>
                        <li className="item">
                            <img className="img" src={finalHint2}/>
                            <p className="hint-text">即刻起，每收到1个<span className="color">「天使」或「陪你看世界」</span></p>
                            <p className="hint-text">即可额外获得1个礼盒，开取新装备！</p>
                            <img className="hint-btn" src={finalBtn} onClick={() => this.state.closeFn()}/>
                        </li>
                    </ul>
                </div>
                <div className="final-pagination"></div>
                <div className="close" onClick={() => this.state.closeFn()}></div>
            </div>
        );
    };

    componentDidMount () {
        this.swiperDOM = this.swiperInit();
    };

    componentDidUpdate () {
        this.swiperDOM.destroy(false);
        this.swiperDOM = this.swiperInit();
    };

    componentWillUnmount () {
        this.swiperDOM.destroy(false);
    };

    swiperInit () {
        return new Swiper('.final-list-wrap',{
            effect : 'fade',
            fadeEffect: {
                crossFade: true,
            },
            pagination: {
                el: '.final-pagination'
            },
            wrapperClass: 'list',
            slideClass: 'item',
        });
    };
};

class Home extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isLogin: false, // 是否登陆
            stage: 1, // 活动状态 预赛 1 决赛 2 结束 3
            endTime: false, // 默认值不能设置为0 因为到达播放时间的时候就是0 单位秒
            isApply: false, // 报名
            isLiving: false, // 直播
            secondStageUnread: false, // 第二阶段，未读提示弹窗
            robChance: 0, // 抢夺的排名
            robUnread: false, // 抢夺机会 弹窗提示
            robbedUnread: {}, // 被抢夺未读提示
            isRobbedUnread: true,
            rank: '暂无', // 排名
            avatar: '', // 头像
            screenName: '未登录',
            score: '???', // 当前分数
            todayScore: '???', // 今天分数
            incrScore: '???', // 翻倍分数
            leftScore: '???', // 还有多少分开启新的礼盒
            equipmentIng: [], // 使用中的装备
            equipmentLeft: [], // 过期的装备
            equipmentRecord: [], //获取的装备记录
            box: 0, // 已经拥有礼盒
            boxUsed: 0, // 已开启礼盒
            boxUnread: false, //提示是否有可开的宝箱
            list: [],
            animateState: false,
            isShowShadow: false,
            isShowEquipmentLayer: false,
            isShowEquipmentGet: false,
            isShowEquipmentUse: false,
            isShowEquipmentHint: false,
            isShowFinalSnatch: false,
            isShowFinalLost: false,
            isShowFinalHint: false,
            isShowFinalSelect: false,
            isShowToast: false,
            toastText: '',
            newCard: {
                rate: 10
            },
            fromId: '', // 被替换卡的Id
            toId: '', // 替换卡的Id
            fromIndex: null, // 被替换卡的位置
            fromId: '',
            isSearch: false, //是否处于搜索状态
            robList: [], // 抢夺弹窗数据
            robObjId: '', // 抢夺卡片的id
            isShowHome: false, // 是否显示首页
            isShowRules: false, // 是否显示规则页面
            isShowResults: false, // 是否显示结果页
            isContinuousOpen: false, // 是否连续开箱子
        };
        this.allList = [];
        this.searchId = '';
        this.timer = null; // 统计定时器
    };

    componentWillMount (nextProps, nextState) {
    };

    componentDidMount () {
        this.getContentData();
        this.timeSave(10);
    };

    componentWillReceiveProps(nextProps) {
    };

    componentWillUnmount() {
        clearTimeout(this.timer);
    };

    equipmentStateTemplate () {
        if (this.state.stage == 2) {
            // 决赛
            if ( this.state.box > this.state.boxUsed ) {
                // 有箱子开
                return (
                    <div className="equipment-title">
                        <div className="equipment-state"
                            style={{backgroundImage: `url('${equipmentState2}')`}}
                            onClick={() => this.showEquipmentGet() }>
                        </div>
                    </div>
                );
            }else{
                // 没箱子开
                return '';
            };
        }else if(this.state.stage == 1) {
            // 预赛
            if (this.state.boxUsed == 3) {
                // 箱子到上限
                return (
                    <div className="equipment-title">
                        <div className="equipment-state"
                            style={{backgroundImage: `url('${equipmentState3}')`}}>
                        </div>
                    </div>
                );
            }else if (this.state.box > this.state.boxUsed) {
                // 有箱子开
                return (
                    <div className="equipment-title">
                        <div className="equipment-state"
                            style={{backgroundImage: `url('${equipmentState2}')`}}
                            onClick={() => this.showEquipmentGet() }>
                        </div>
                    </div>
                );
            }else{
                return (
                    <div className="equipment-title">
                        <div className="equipment-state"
                            style={{backgroundImage: `url('${equipmentState1}')`}}>
                            <p className="text">还需<span className="text-color">{this.state.leftScore}</span></p>
                            <p className="text">装备值可开启</p>
                        </div>
                    </div>
                );
            };
        };
    };

    validTemplate () {
        let list = [];
        let length = this.state.equipmentIng.length;
        if (length == 3) {
            // 使用中的装备满3个
            list = this.state.equipmentIng.map((item, index, array) => {
                return (
                    <li className="item" key={index}>
                        <div className="intro">
                            <p className="title">{`+${item.rate}%`}</p>
                            <p className="text">积分加成</p>
                        </div>
                        {
                            this.state.equipmentLeft.length > 0
                                ?<img className="btn" src={combatBtnIcon} onClick={() => this.showEquipmentUse(item.id, index)}/>
                                :''
                        }
                    </li>
                );
            });
            return list;
        };
        if ( length > 0 ) {
            // 使用中的装备不满3个
            list = this.state.equipmentIng.map((item, index, array) => {
                // 有待使用的装备才显示替换按钮
                return (
                    <li className="item" key={index}>
                        <div className="intro">
                            <p className="title">{`+${item.rate}%`}</p>
                            <p className="text">积分加成</p>
                        </div>
                        {
                            this.state.equipmentLeft.length > 0
                                ?<img className="btn" src={combatBtnIcon} onClick={() => this.showEquipmentUse(item.id, index)}/>
                                :''
                        }
                    </li>
                );
            });

            if (this.state.equipmentLeft.length > 0) {
                let dom = (
                    <li className={`item ${this.state.equipmentIng == 3? 'hide': ''}`} key={Math.floor(Math.random()*100)}>
                        <div className="intro" onClick={() => this.showEquipmentUse()}>
                            <p className="title">{'使用'}</p>
                            <p className="title">{'装备'}</p>
                        </div>
                        {
                            this.state.equipmentLeft.length > 0
                                ?<img className="btn" src={useBtn} onClick={() => this.showEquipmentUse()}/>
                                :''
                        }
                    </li>
                );
                list.push(dom);
            };

            return list;
        };
        if (length == 0 && this.state.equipmentLeft.length > 0) {
            // 没有使用中的装备，但是有待使用的装备
            let dom = (
                <li className={`item ${this.state.equipmentIng.length == 3? 'hide': ''}`} key={10}>
                    <div className="intro" onClick={() => this.showEquipmentUse()}>
                        <p className="title">{'使用'}</p>
                        <p className="title">{'装备'}</p>
                    </div>
                    {
                        this.state.equipmentLeft.length > 0
                            ?<img className="btn" src={useBtn} onClick={() => this.showEquipmentUse()}/>
                            :''
                    }
                </li>
            );
            list.push(dom);
            return list;
        };
        // 没有装备，没有记录
        let dom = (
            <div className="default active" key={0}>
                <p className="hint-title">暂无</p>
                <p className="hint-text">快去赢取装备吧～</p>
            </div>
        );
        list.push(dom);
        return list;
    };

    finalSelectTemplate (list) {
        try{
            return list.map((item, index, array) => {
                return (
                    <li className={`item ${this.state.robObjId == item.id? 'active': ''}`} key={index}
                        onClick={() => this.checkRobObj(item.id)}>
                        <p className="hint-title">{`+${item.rate}%`}</p>
                        <p className="hint-text">积分加成</p>
                    </li>
                );
            });
        }catch(e){
            return '';
        };
    };

    render () {
        return (
            <div className={`home-page ${/iPhone/.test(navigator.userAgent)? '': 'android'}`}>
                <div className={`rules-btn ${this.state.isShowRules? 'active': ''}`}
                    onClick={() => this.showEquipmentLayer()}>
                </div>
                    <div className={`home`}>
                        <HeaderComponent />
                        <RulesComponent />
                        <div className="my-data-wrap">
                            <CountDownComponent
                                endTime={this.state.endTime}/>
                            <MyDataComponent
                                {...this.state}
                                login={this.login.bind(this)}
                                redirectFn={this.redirect.bind(this)}/>
                        </div>
                        <StarListComponent
                            list={this.state.list}
                            robChance={this.state.robChance}
                            redirect={this.redirect.bind(this)}
                            changeFn={this.changeSearchId.bind(this)}
                            searchFn={this.searchStar.bind(this)}
                            searchGoBackFn={this.searchGoBack.bind(this)}
                            finalSelectFn={this.showFinalSelect.bind(this)}
                            searchId={this.state.searchId}
                            isSearch={this.state.isSearch}
                            uid={this.state.uid}/>
                        <div className="floor">
                            <p className="hint">本活动最终解释权归美拍所有</p>
                        </div>
                        <GoTop />
                        {
                            this.state.isApply
                                ?
                                    <div className="equipment-btn shake" onClick={() => this.showEquipmentLayer()}>
                                        {
                                            this.state.box - this.state.boxUsed > 0
                                                ?<img src={dotIcon} className="dot"/>
                                                :''
                                        }
                                    </div>
                                :''
                        }
                        <div className={`equipments-wrap ${this.state.isShowShadow? 'active': ''}`}>
                            <div className={`equipment-layer ${this.state.isShowEquipmentLayer? 'active': ''}`}>
                                <div className={`equipment-mian ${(this.state.stage == 2) && (this.state.box <= this.state.boxUsed)? 'top': ''}`}>
                                    {this.equipmentStateTemplate()}
                                    {
                                        this.state.box > this.state.boxUsed
                                            ?<p className="box-used-hint">请及时开取，今晚24点过期</p>
                                            :''
                                    }
                                    <ul className="valid">
                                        {this.validTemplate()}
                                    </ul>
                                    <EquipmentAcquire
                                        list={this.state.equipmentRecord}/>
                                    <div className="close" onClick={() => this.hideEquipmentLayer()}></div>
                                </div>
                                <div className="equipment-bottom"></div>
                            </div>
                            <div className={`equipment-use ${this.state.isShowEquipmentUse? 'active': ''}`}>
                                <div className="use-head">
                                    <div className="close" onClick={() => this.hideEquipmentUse()}></div>
                                </div>
                                <div className="use-list-wrap">
                                    <ul className={`use-list ${this.state.equipmentLeft.length <= 2? 'center': ''}`}>
                                        {
                                            this.state.equipmentRecord.map((item, index, array) => {
                                                if (!item.ing && item.effective) {
                                                    return (
                                                        <li className="item" key={index} onClick={() => {
                                                            this.clickItemEquipmentUse(item, index);
                                                        }}>
                                                            <p className="hint-title">{`+${item.rate}%`}</p>
                                                            <p className="hint-text">积分加成</p>
                                                        </li>
                                                    );
                                                }else{
                                                    return '';
                                                };
                                            })
                                        }
                                    </ul>
                                </div>
                                <div className="use-floor"></div>
                            </div>
                            <div className={`equipment-get ${this.state.isShowEquipmentGet? 'active': ''}`}>
                                <div className="card">
                                    <p className="explain-title">{`+${this.state.newCard.rate}%`}</p>
                                    <p className="explain-text">积分加成</p>
                                </div>
                                <div className="text">
                                    <span className="color">生效期间，可使</span><span className="color" >新增装备值</span><span className="color">{`加成${this.state.newCard.rate}%，`}</span><span className="color">{`即翻${(this.state.newCard.rate/100)+1}倍`}</span>
                                </div>
                                <div className="btn-area">
                                    <div className="btn" onClick={() => this.storeEquipment('已存入「我的装备-领取记录」中~')}></div>
                                    <div className="btn" onClick={() => this.useEquipment('卡片已生效')}></div>
                                </div>
                                <div className="close" onClick={() => this.storeEquipment() }></div>
                            </div>
                            <div className={`equipment-hint ${this.state.isShowEquipmentHint? 'active': ''}`}>
                                <div className="close" onClick={() => this.hideEquipmentHint()}></div>
                                <div className="open" onClick={() => this.hideEquipmentHint(1)}></div>
                            </div>
                            <FinalHint
                                isShow={this.state.isShowFinalHint}
                                closeFn={this.hideFinalHint.bind(this)}/>
                            <div className={`final-select ${this.state.isShowFinalSelect? 'active': ''}`}>
                                <ul className="select-list">
                                    {this.finalSelectTemplate(this.state.robList)}
                                </ul>
                                <div className="btn-area">
                                    <div className="btn" onClick={() => this.useRobObj(1)}></div>
                                    <div className="btn" onClick={() => this.useRobObj()}></div>
                                </div>
                                <div className="close" onClick={() => this.hideFinalSelect()}></div>
                            </div>
                            <div className={`final-lost ${this.state.isShowFinalLost? 'active': ''}`}>
                                <p className="hint-text">你离开的这段时间，<span className="color">{`「${this.state.robbedUnread.screenName}」`}</span></p>
                                <p className="hint-text">超越并抢走了你<span className="color">{`【+${this.state.robbedUnread.rate}%积分加成卡】`}</span>~</p>
                                <p className="hint-text">别气馁，马上去开播反超TA！</p>
                                <div className="close" onClick={() => this.hideFinalLost()}></div>
                                <div className="agree" onClick={() => this.hideFinalLost()}></div>
                            </div>
                            <div className={`final-snatch ${this.state.isShowFinalSnatch? 'active': ''}`}>
                                <div className="close" onClick={() => this.hideFinalSnatch()}></div>
                                <div className="agree" onClick={() => this.goToMyListItem()}></div>
                            </div>
                        </div>
                        {
                            this.state.isShowToast
                                ?<div className="page-toast">{this.state.toastText}</div>
                                :''
                        }
                    </div>
            </div>
        );
    };
    /* ----------------------------获取数据------------------------------------------ */
    getContentData () {
        ajax({
            method: 'get',
            url: '/equipment2018/index_data',
        }).then(res => {
            console.log(res);
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            // 第三阶段的数据对象不一样所以需要判断
            if (res.stage == 3 ){
                this.setState({
                    isLogin: res.isLogin,
                    stage: res.stage,
                    endTime: res.leftTime * 1000 || false,
                    list: res.list,
                }, () =>{
                    //this.showLayer();
                    this.showStage();
                });
            }else{
                // let arr = [
                //     {id: "334", rate: 15, uid: 1561522488, ing: true, expire_at: 1526301000, effective: true},
                //     {id: "334", rate: 15, uid: 1561522488, ing: true, expire_at: 1526301000, effective: true},
                //     {id: "334", rate: 15, uid: 1561522488, ing: true, expire_at: 1526301000, effective: true},
                // ];
                // let arr1 = [
                //     //{id: "334", rate: 15, uid: 1561522488, ing: false, expire_at: 1526301000, effective: true},
                //     //{id: "334", rate: 15, uid: 1561522488, ing: false, expire_at: 1526301000, effective: true},
                // ];
                // let arr2 = [
                //     {id: "334", rate: 15, uid: 1561522488, ing: true, expire_at: 1526301000, effective: true},
                //     {id: "334", rate: 15, uid: 1561522488, ing: true, expire_at: 1526301000, effective: true},
                //     {id: "334", rate: 15, uid: 1561522488, ing: true, expire_at: 1526301000, effective: true},
                //     //{id: "334", rate: 15, uid: 1561522488, ing: false, expire_at: 1526301000, effective: true},
                //     //{id: "334", rate: 15, uid: 1561522488, ing: false, expire_at: 1526301000, effective: true},
                // ];
                this.setState({
                    isLogin: res.isLogin,
                    stage: res.stage,
                    endTime: res.leftTime * 1000 || false,
                    isApply: res.user.isApply,
                    isLiving: res.user.isLiving,
                    secondStageUnread: res.user.secondStageUnread,
                    robChance: res.user.robChance,
                    robUnread: res.user.robUnread,
                    robbedUnread: res.user.robbedUnread,
                    rank: res.user.rank,
                    avatar: res.user.avatar,
                    screenName: res.user.screenName,
                    score: res.user.score,
                    todayScore: res.user.todayScore,
                    incrScore: res.user.incrScore,
                    leftScore: res.user.leftScore,
                    equipmentIng: res.user.equipmentIng,
                    equipmentLeft: res.user.equipmentLeft,
                    equipmentRecord: res.user.equipmentRecord,
                    box: res.user.box,
                    boxUsed: res.user.boxUsed,
                    list: res.list,
                    boxUnread: res.user.boxUnread,
                    uid: res.user.uid
                }, () =>{
                    // this.showLayer();
                    this.showStage();
                    this.allList = res.list;
                });
            };
        });
    };
    // 登陆 && 报名
    login () {
        if (!this.state.isLogin) {
            // 未登陆
            let ua = window.navigator.userAgent.toLowerCase();
            if (/(com.meitu.myxj|com.meitu.meiyancamera)/gi.test(ua)) {
                this.showToast('需退出本页面登录美颜相机账号噢~', 1500);
                return;
            }else if(/(com.meitu.mtxx)/gi.test(ua)) {
                this.showToast('需退出本页面登录美图秀秀账号噢~', 1500);
                return;
            };
            window.location.href = `/login/web_login?login_redirect=${encodeURIComponent(location.href)}`;
            return;
        };
        if (!this.state.isApply) {
            // 未报名
            this.statistics('拆礼盒-主页->报名');
            ajax({
                method: 'get',
                url: '/equipment2018/apply',
            }).then(res => {
                if (res.code != 0) {
                    this.showToast(res.msg);
                    return;
                };
                this.showToast('报名成功');
                // 报名成功 刷新页面
                // 不在刷新页面而是直接重新请求数据
                // window.location.href = window.location.href+'?time='+((new Date()).getTime());
                this.getContentData();
            });
        };
    };
/*
    目前app跳转协议和地址
        美拍个人页 mtmv://user?id=1587510663
        美拍直播页 mtmv://lives?id=988555140

        秀秀个人页 mtmv://user?id=1587510663
        秀秀直播页 mtlive://lives?id=988555140
*/
    // 跳转直播或者个人页
    redirect (type, id) {
        let ua = window.navigator.userAgent.toLowerCase();
        let uas = {
            iOS: /(iPhone|iPad|iPod|iOS)/gi.test(ua),
            Android: /android|adr/gi.test(ua),
            Mobile: /(iPhone|iPad|iPod|iOS|Android|adr|Windows Phone|SymbianOS)/gi.test(ua),
            Weibo: /(weibo)/gi.test(ua),
            WeChat: ua.match(/MicroMessenger/gi) == 'micromessenger' ? true : false,
            QQ: /qq\//gi.test(ua),
            Qzone: ua.indexOf('qzone/') !== -1,
            Meitu: /(com.meitu)/gi.test(ua),
            Meipai: /meipaimv|meipai|com.meitu.mtmv/ig.test(ua),
            Meipu: /com.meitu.meipu/ig.test(ua),
            Xiuxiu: /(com.meitu.mtxx)/gi.test(ua),
            // ios: com.meitu.myxj, android: com.meitu.meiyancamera
            Meiyan: /(com.meitu.myxj|com.meitu.meiyancamera)/gi.test(ua),
            Makeup: /com.meitu.makeup/ig.test(ua),
            Selfilecity: /com.meitu.wheecam/ig.test(ua),
            Beautyme: /com.meitu.zhi.beauty/ig.test(ua),
            Shanliao: /(com.meitu.shanliao|com.meitu.testwheetalk)/ig.test(ua),
            Twitter: /Twitter/ig.test(ua),
            Facebook: /fbav/ig.test(ua)
        };
        if (this.state.isLiving) {
            this.showToast('对不起，由于您当前正在开播，暂时无法访问其他直播间或个人主页', 2500);
            return;
        };
        switch (type) {
            case 'user':
                // id为0不跳转
                if (id == 0 || !id) return;
                this.statistics('拆礼盒-主页->榜单跳主播个人页');
                if (/meipaimv|meipai|com.meitu.mtmv/ig.test(ua)) {
                    // 美拍个人页
                    location.href = `mtmv://user?id=${id}`;
                }else if (/(com.meitu.mtxx)/gi.test(ua)) {
                    // 秀秀个人页
                    location.href = `https://www.meipai.com/user/${id}`;
                }else {
                    // 未知个人页
                    location.href = `https://www.meipai.com/user/${id}`;
                };
            break;
            case 'live':
                this.statistics('拆礼盒-主页->榜单跳主播直播页');
                if (/meipaimv|meipai|com.meitu.mtmv/ig.test(ua)) {
                    // 美拍直播
                    location.href = `mtmv://lives?id=${id}`;
                }else if (/(com.meitu.mtxx)/gi.test(ua)) {
                    // 秀秀直播
                    location.href = `https://www.meipai.com/live/${id}`;
                }else {
                    // 未知直播
                    location.href = `https://www.meipai.com/live/${id}`;
                };
            break;
        };
    };
    // 显示 && 隐藏    我的装备
    showEquipmentLayer () {
        if (!this.state.isApply || !this.state.isLogin) {
            this.login();
            return;
        };
        this.statistics('拆礼盒-主页->我的装备');
        this.setState({
            isShowShadow: true,
            isShowEquipmentLayer: true,
        });
    };
    hideEquipmentLayer () {
        this.setState({
            isShowShadow: false,
            isShowEquipmentLayer: false,
        },() => {
            this.showLayer();
        });
    };
    // 开启 && 关闭 选装备弹窗
    showEquipmentUse (id, index) {
        this.setState({
            isShowEquipmentUse: true,
            fromId: id,
            fromIndex: index
        });
    };
    hideEquipmentUse () {
        this.setState({
            isShowEquipmentUse: false,
            fromId: '',
            fromIndex: null
        },() => {
            this.showLayer();
        });
    };
    // 装备选择窗口 判断是替换还是选择
    clickItemEquipmentUse (item, index) {
        if (typeof this.state.fromId == 'undefined'){
            this.statistics('拆礼盒-主页->使用装备');
            // 使用
            ajax({
                method: 'post',
                url: '/equipment2018/use_equipment',
                data: {
                    id: item.id
                },
            }).then((res) => {
                if (res.code != 0) {
                    this.showToast(res.msg);
                    return;
                };
                ajax({
                    method: 'get',
                    url: '/equipment2018/index_data',
                }).then(res => {
                    console.log(res);
                    if (res.code != 0) {
                        this.showToast(res.msg);
                        return;
                    };
                    this.setState({
                        isLogin: res.isLogin,
                        stage: res.stage,
                        endTime: res.leftTime * 1000 || false,
                        isApply: res.user.isApply,
                        isLiving: res.user.isLiving,
                        secondStageUnread: res.user.secondStageUnread,
                        robChance: res.user.robChance,
                        rank: res.user.rank,
                        avatar: res.user.avatar,
                        screenName: res.user.screenName,
                        score: res.user.score,
                        todayScore: res.user.todayScore,
                        incrScore: res.user.incrScore,
                        leftScore: res.user.leftScore,
                        equipmentIng: res.user.equipmentIng,
                        equipmentLeft: res.user.equipmentLeft,
                        equipmentRecord: res.user.equipmentRecord,
                        box: res.user.box,
                        boxUsed: res.user.boxUsed,
                        list: res.list,
                        uid: res.user.uid
                    }, () => {
                        // 关闭礼盒弹窗
                        this.hideEquipmentUse();
                        this.showToast('使用成功');
                    });
                    this.allList = res.list;
                });
            });
        }else{
            this.statistics('拆礼盒-主页->替换装备');
            // 替换
            this.replaceEquipment(item, index);
        };
    };
    // 替换装备
    replaceEquipment (item, index) {
        ajax({
            method: 'post',
            url: '/equipment2018/replace_equipment',
            data: {
                id: item.id,
                replaced_id: this.state.fromId
            }
        }).then(res => {
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            ajax({
                method: 'get',
                url: '/equipment2018/index_data',
            }).then(res => {
                console.log(res);
                if (res.code != 0) {
                    this.showToast(res.msg);
                    return;
                };
                this.setState({
                    isLogin: res.isLogin,
                    stage: res.stage,
                    endTime: res.leftTime * 1000 || false,
                    isApply: res.user.isApply,
                    isLiving: res.user.isLiving,
                    secondStageUnread: res.user.secondStageUnread,
                    robChance: res.user.robChance,
                    rank: res.user.rank,
                    avatar: res.user.avatar,
                    screenName: res.user.screenName,
                    score: res.user.score,
                    todayScore: res.user.todayScore,
                    incrScore: res.user.incrScore,
                    leftScore: res.user.leftScore,
                    equipmentIng: res.user.equipmentIng,
                    equipmentLeft: res.user.equipmentLeft,
                    equipmentRecord: res.user.equipmentRecord,
                    box: res.user.box,
                    boxUsed: res.user.boxUsed,
                    list: res.list,
                    uid: res.user.uid
                },() => {
                    this.hideEquipmentUse();
                    this.showToast('替换成功');
                });
                this.allList = res.list;
            });
        });
    };
    // 开礼盒
    showEquipmentGet () {
        this.statistics('拆礼盒-主页->打开礼盒');
        ajax({
            method: 'get',
            url: '/equipment2018/open_box',
        }).then(res => {
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            // boxUsed 开启盒子数量 +1  并且显示弹窗
            this.setState({
                isShowShadow: true,
                isShowEquipmentGet: true,
                boxUsed: this.state.boxUsed + 1,
                newCard: res,
            });
        });
    };
    hideEquipmentGet () {
        this.setState({
            isShowEquipmentGet: false,
        });
    };
    // 存入我的装备记录
    storeEquipment (text) {
        ajax({
            method: 'get',
            url: '/equipment2018/index_data',
        }).then(res => {
            console.log(res);
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            this.setState({
                isLogin: res.isLogin,
                stage: res.stage,
                endTime: res.leftTime * 1000 || false,
                isApply: res.user.isApply,
                isLiving: res.user.isLiving,
                secondStageUnread: res.user.secondStageUnread,
                robChance: res.user.robChance,
                rank: res.user.rank,
                avatar: res.user.avatar,
                screenName: res.user.screenName,
                score: res.user.score,
                todayScore: res.user.todayScore,
                incrScore: res.user.incrScore,
                leftScore: res.user.leftScore,
                equipmentIng: res.user.equipmentIng,
                equipmentLeft: res.user.equipmentLeft,
                equipmentRecord: res.user.equipmentRecord,
                box: res.user.box,
                boxUsed: res.user.boxUsed,
                list: res.list,
                uid: res.user.uid,
                newCard: {},
            }, () => {
                if (text) {
                    this.showToast(text);
                };
                if (this.state.isContinuousOpen) {
                    // 连续弹窗开箱子模式
                    this.setState({
                        // 关闭礼盒弹窗
                        // 如果 EquipmentLayer 没有开启那就直接关闭背景蒙层
                        isShowShadow: this.state.isShowEquipmentLayer? true: false,
                        isShowEquipmentGet: false
                    }, () => {
                        this.showLayer();
                    });
                }else {
                    // 单个箱子模式
                    this.setState({
                        // 关闭礼盒弹窗
                        // 如果 EquipmentLayer 没有开启那就直接关闭背景蒙层
                        isShowShadow: this.state.isShowEquipmentLayer? true: false,
                        isShowEquipmentGet: false
                    });
                };
            });
            this.allList = res.list;
        });
    };
    // 使用
    useEquipment (text) {
        ajax({
            method: 'post',
            url: '/equipment2018/use_equipment',
            data: {
                id: this.state.newCard.id
            },
        }).then((res) => {
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            ajax({
                method: 'get',
                url: '/equipment2018/index_data',
            }).then(res => {
                console.log(res);
                if (res.code != 0) {
                    this.showToast(res.msg);
                    return;
                };
                this.setState({
                    isLogin: res.isLogin,
                    stage: res.stage,
                    endTime: res.leftTime * 1000 || false,
                    isApply: res.user.isApply,
                    isLiving: res.user.isLiving,
                    secondStageUnread: res.user.secondStageUnread,
                    robChance: res.user.robChance,
                    rank: res.user.rank,
                    avatar: res.user.avatar,
                    screenName: res.user.screenName,
                    score: res.user.score,
                    todayScore: res.user.todayScore,
                    incrScore: res.user.incrScore,
                    leftScore: res.user.leftScore,
                    equipmentIng: res.user.equipmentIng,
                    equipmentLeft: res.user.equipmentLeft,
                    equipmentRecord: res.user.equipmentRecord,
                    box: res.user.box,
                    boxUsed: res.user.boxUsed,
                    list: res.list,
                    uid: res.user.uid,
                }, () => {
                    if (text) {
                        this.showToast(text);
                    };
                    if (this.state.isContinuousOpen) {
                        // 连续开箱子模式
                        this.setState({
                            // 关闭礼盒弹窗
                            // 如果 EquipmentLayer 没有开启那就直接关闭背景蒙层
                            isShowShadow: this.state.isShowEquipmentLayer? true: false,
                            isShowEquipmentGet: false
                        }, () => {
                            this.showLayer();
                        });
                    }else {
                        // 单个箱子模式
                        this.setState({
                            // 关闭礼盒弹窗
                            // 如果 EquipmentLayer 没有开启那就直接关闭背景蒙层
                            isShowShadow: this.state.isShowEquipmentLayer? true: false,
                            isShowEquipmentGet: false,
                        });
                    };
                });
                this.allList = res.list;
            });
        });
    };
    // 开启 && 关闭 获取装备提示
    showEquipmentHint () {
        this.setState({
            isShowEquipmentHint: true,
            isShowShadow: true,
        });
    };
    hideEquipmentHint (type){
        if(type){
            // 关闭提示直接打开礼盒
            this.setState({
                isShowEquipmentHint: false,
                isShowShadow: false,
            },() => {
                this.showEquipmentGet();
            });
        }else{
            // 直接关闭提示
            this.setState({
                isShowEquipmentHint: false,
                isShowShadow: false,
                boxUnread: false
            },() => {
                this.showLayer();
            });
        };
    };
    // 搜索输入框
    changeSearchId (e) {
        // this.setState({
        //     searchId: e.target.value
        // });
    };
    // 搜索按钮
    searchStar () {
        this.statistics('拆礼盒-主页->搜索');
        let serchId = document.getElementsByClassName('search')[0].value;
        ajax({
            method: 'get',
            url: '/equipment2018/search_user',
            data: {
                uid: serchId,
            }
        }).then(res => {
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            this.setState({
                list: [res],
                searchId: '',
                isSearch: true
            });
        });
    };
    // 搜索返回
    searchGoBack () {
        this.statistics('拆礼盒-主页->搜索返回');
        document.getElementsByClassName('search')[0].value = '';
        this.setState({
            list: this.allList,
            isSearch: false
        });
    };
    // 开启 && 关闭 决赛提示
    showFinalHint () {
        this.setState({
            isShowShadow: true,
            isShowFinalHint: true,
        });
    };
    hideFinalHint () {
        this.setState({
            isShowShadow: false,
            isShowFinalHint: false,
        },() => {
            this.showLayer();
        });
    };
    // 显示 && 隐藏  抢装备提示
    showFinalSnatch () {
        this.setState({
            isShowShadow: true,
            isShowFinalSnatch: true,
        });
    };
    hideFinalSnatch () {
        this.setState({
            isShowShadow: false,
            isShowFinalSnatch: false,
        },() => {
            this.showLayer();
        });
    };
    // 去抢装备 定位到列表
    goToMyListItem () {
        this.setState({
            isShowShadow: false,
            isShowFinalSnatch: false,
        },() => {
            this.showLayer();
            let myOffsetTop = document.getElementsByClassName('is-my')[0].offsetTop - 500;
            $("body,html").animate({scrollTop: myOffsetTop}, 500);
        });
    };
    // 显示 && 隐藏  被抢提示
    showFinalLost () {
        this.setState({
            isShowShadow: true,
            isShowFinalLost: true,
        });
    };
    hideFinalLost () {
        this.setState({
            isShowShadow: false,
            isShowFinalLost: false,
        },() => {
            this.showLayer();
        });
    };
    // 显示 && 隐藏  抢夺选择
    showFinalSelect (e, uid) {
        e.nativeEvent.stopImmediatePropagation();
        ajax({
            method: 'get',
            url: '/equipment2018/search_equipment',
            data: {
                uid: uid,
            }
        }).then(res => {
            if (res.code != 0) {
                this.showToast(res.msg);
                return;
            };
            // 没有可抢夺装备时 不出现弹窗 考虑添加
            if (res.ing.length == 0) {
                this.setState({
                    robList: res.ing,
                }, () => this.showToast('对方没有可被抢夺的装备~'));
            }else{
                this.setState({
                    robList: res.ing,
                    isShowShadow: true,
                    isShowFinalSelect: true,
                });
            };
        });
    };
    hideFinalSelect () {
        this.setState({
            isShowShadow: false,
            isShowFinalSelect: false,
        },() => {
            this.showLayer();
        });
    };
    // 选中抢夺对象
    checkRobObj (id) {
        this.setState({
            robObjId: id
        });
    };
    // 存入 && 使用 抢夺的装备
    useRobObj (type) {
        if (!this.state.robObjId) {
            // 没有选中装备
            this.showToast('请选择要抢夺的装备');
            return;
        };
        this.statistics('拆礼盒-主页->抢装备');
        // type 1 存入  其他情况是 使用
        if ((this.state.equipmentIng.length == 3 && type != 1) || type == 1) {
            // 情况1 使用装备已经满了，点击使用，直接调用抢夺接口然后放入背包
            // 情况2 点击存入背包
            ajax({
                method: 'post',
                url: '/equipment2018/rob',
                data: {
                    id: this.state.robObjId,
                }
            }).then(res => {
                if (res.code != 0) {
                    this.showToast(res.msg);
                    return;
                };
                ajax({
                    method: 'get',
                    url: '/equipment2018/index_data',
                }).then(res => {
                    console.log(res);
                    if (res.code != 0) {
                        this.showToast(res.msg);
                        return;
                    };
                    this.setState({
                        isLogin: res.isLogin,
                        stage: res.stage,
                        endTime: res.leftTime * 1000 || false,
                        isApply: res.user.isApply,
                        isLiving: res.user.isLiving,
                        secondStageUnread: res.user.secondStageUnread,
                        robChance: res.user.robChance,
                        robUnread: res.user.robUnread,
                        robbedUnread: res.user.robbedUnread,
                        rank: res.user.rank,
                        avatar: res.user.avatar,
                        screenName: res.user.screenName,
                        score: res.user.score,
                        todayScore: res.user.todayScore,
                        incrScore: res.user.incrScore,
                        leftScore: res.user.leftScore,
                        equipmentIng: res.user.equipmentIng,
                        equipmentLeft: res.user.equipmentLeft,
                        equipmentRecord: res.user.equipmentRecord,
                        box: res.user.box,
                        boxUsed: res.user.boxUsed,
                        list: res.list,
                        boxUnread: res.user.boxUnread,
                        uid: res.user.uid,
                        // 关闭抢夺弹窗
                        isShowShadow: false,
                        isShowFinalSelect: false,
                        // 清理抢夺id
                        robObjId: '',
                    },() => {
                        if (type==1) {
                            this.showToast('已存入「我的装备-领取记录」中~');
                        }else{
                            this.showToast('你当前生效卡片已达上限，已存入「我的装备」中，快去腾位置吧！', 2500);
                        };
                    });
                    this.allList = res.list;
                });
            });
            return;
        };

        if (type != 1) {
            // 装备未装备满 直接使用
            ajax({
                method: 'post',
                url: '/equipment2018/rob',
                data: {
                    id: this.state.robObjId,
                }
            }).then(res => {
                if (res.code != 0) {
                    this.showToast(res.msg);
                    return;
                };
                ajax({
                    method: 'post',
                    url: '/equipment2018/use_equipment',
                    data: {
                        id: this.state.robObjId,
                    },
                }).then((res) => {
                    if (res.code != 0) {
                        this.showToast(res.msg);
                        return;
                    };
                    ajax({
                        method: 'get',
                        url: '/equipment2018/index_data',
                    }).then(res => {
                        console.log(res);
                        if (res.code != 0) {
                            this.showToast(res.msg);
                            return;
                        };
                        this.setState({
                            isLogin: res.isLogin,
                            stage: res.stage,
                            endTime: res.leftTime * 1000 || false,
                            isApply: res.user.isApply,
                            isLiving: res.user.isLiving,
                            secondStageUnread: res.user.secondStageUnread,
                            robChance: res.user.robChance,
                            robUnread: res.user.robUnread,
                            robbedUnread: res.user.robbedUnread,
                            rank: res.user.rank,
                            avatar: res.user.avatar,
                            screenName: res.user.screenName,
                            score: res.user.score,
                            todayScore: res.user.todayScore,
                            incrScore: res.user.incrScore,
                            leftScore: res.user.leftScore,
                            equipmentIng: res.user.equipmentIng,
                            equipmentLeft: res.user.equipmentLeft,
                            equipmentRecord: res.user.equipmentRecord,
                            box: res.user.box,
                            boxUsed: res.user.boxUsed,
                            list: res.list,
                            boxUnread: res.user.boxUnread,
                            uid: res.user.uid,
                            // 关闭抢夺弹窗
                            isShowShadow: false,
                            isShowFinalSelect: false,
                            // 清理抢夺id
                            robObjId: '',
                        },() => {
                            this.showToast('装备已使用');
                        });
                        this.allList = res.list;
                    });
                });
            });
        };
    };
    // 阶段判断
    showStage () {
        if (this.state.stage == 3) {
            // 结果页 第三阶段
            // this.setState({
            //     isShowHome: false, // 是否显示首页
            //     isShowResults: true, // 是否显示结果页
            // });
            this.props.router.replace({pathname: '/results'})
        }else{
            // 首页 第一 二 阶段
            this.setState({
                isShowHome: true, // 是否显示首页
                isShowResults: false, // 是否显示结果页
            },() => this.showLayer());
        };
    };
    // 弹窗显示顺序
    showLayer () {
        if (this.state.boxUnread) {
            // 得到宝箱提示第一位
            if (this.state.box - this.state.boxUsed >= 2) {
                // 有2个以上的箱子可以开，进入连续开箱子模式
                this.setState({
                    isContinuousOpen: true
                }, () => {
                    this.showEquipmentHint();
                });
            }else{
                this.setState({
                    boxUnread: false,
                    isContinuousOpen: false,
                }, () => {
                    this.showEquipmentHint();
                });
            };
            return;
        };
        if (this.state.secondStageUnread) {
            // 第二阶段提示
            this.setState({
                secondStageUnread: false
            }, () => {
                this.showFinalHint();
            });
            return;
        };
        if (this.state.robbedUnread.uid && this.state.isRobbedUnread) {
            // 被抢夺提示
            this.setState({
                isRobbedUnread: false
            }, () => {
                this.showFinalLost();
            });
            return;
        };
        if (this.state.robUnread) {
            // 抢夺机会提示
            this.setState({
                robUnread: false
            }, () => {
                this.showFinalSnatch();
            });
            return;
        };
    };
    // toast 显示
    showToast (text, time) {
        let newTime = time || 1000;
        this.setState({
            isShowToast: true,
            toastText: text || this.state.toastText
        });
        setTimeout(() => {
            this.setState({
                isShowToast: false
            });
        },newTime);
    };
    // 统计
    statistics (eventname, action = 'click', optLabel = '', optValue = '', op = '') {
        console.log(1)
        try{
            window._czc.push(['_trackEvent', eventname, action, optLabel, optValue]);
        }catch(e){

        };
    };
    // 停留时间统计
    timeSave (t) {
        if (t >= 120) {
            return;
        };
        console.log('主页')
        this.timer = setTimeout(() => {
            this.statistics(`拆礼盒-主页->停留大于${t}秒`);
            clearTimeout(this.timer);
            this.timeSave(t + 10);
        }, 10000);
    };
};

export default Home;



