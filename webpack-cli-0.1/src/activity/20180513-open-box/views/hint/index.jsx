import './index.scss';

import React from 'react';
import { Link, hashHistory } from 'react-router';

class Hint extends React.Component {
    constructor (props) {
        super(props);
    };

    componentWillMount (nextProps, nextState) {
        // 判断是不是在pc 打开
        if (sessionStorage.getItem('isWeb') == 'false'? true: false) {
            hashHistory.push('/');
            return;
        };
    };

    render () {
        return (
            <div className={`hint-page`}>
                <div className="popup">
                    404
                </div>
            </div>
        );
    };
};

export default Hint;