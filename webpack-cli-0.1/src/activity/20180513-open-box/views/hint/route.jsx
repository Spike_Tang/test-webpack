import Hint from './index.jsx';

export default (store) => ({
    path: '/hint',
    component: Hint,
    onEnter: (params, replace) => {
    	// 验证
    	if (!window.globalData.agent.isWeb) {
    		replace('/home');
    	};
    },
    onLeave: (params, replace) => {

    }
});