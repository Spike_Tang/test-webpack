import './index.scss';

import React from 'react';
import { Link, hashHistory } from 'react-router';
import { differentiateTwoJSON } from 'utilsPath/string-function.js';
 
import rulesBackImg from 'imgPath/rules-back.png';
import rulesTitleImg from 'imgPath/rules-title.png';
import preliminaryImg from 'imgPath/preliminary.png';
import finalImg from 'imgPath/final.png';


class RulesContest extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            isFinals: props.isFinals || false // false 预赛 true 决赛
        };

        this.oldProps = props;
    };
    componentWillMount() {
        
    };
    shouldComponentUpdate(nextProps, nextState) {
        // 拦截无效的render
        let isDiffProps = differentiateTwoJSON(this.oldProps, nextProps);
        let isDiffState = differentiateTwoJSON(this.state, nextState);
        if ( isDiffProps || isDiffState ) {
            // 无论是因为 props 改变还是 state 改变 都进行oldProps赋值
            this.oldProps = nextProps;
            return true;
        };
        return false;
    };
    render () {
        console.log('RulesContest');
        return (
            <div className="rules-contest-wrap">
                <div className="click-slide">
                    <div className="left" onClick={() => this.setFinals(false)}></div>
                    <div className="right" onClick={() => this.setFinals(true)}></div>
                </div>
                <div className={`click-area ${this.state.isFinals? '': 'active'}`}></div>
                <img src={finalImg} className={`rules-contest ${this.state.isFinals? 'active': ''}`}/>
                <img src={preliminaryImg} className={`rules-contest ${this.state.isFinals? '': 'active'}`}/>
            </div>
        );    
    };
    setFinals (bool) {
        this.setState({
            isFinals: bool || false
        }, () => {
            // this.event = new CustomEvent('mytab', {detail: {isShow: this.state.isFinals}});
            // window.dispatchEvent(this.event);
        });
    };
};

class Rules extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            isFinals: false
        };
        this.timer = null;
    };
    componentDidMount() {
        this.timeSave(10);
    };
    componentWillUnmount() {
        clearTimeout(this.timer);
    };
    render () {
        console.log('rules');
        return (
            <div className={`rules-page`}>
                <div className="header" onClick={() => {this.setState({isFinals: !this.state.isFinals})}}></div>
                <div className="main">
                        <div className="rules-title">
                            <img src={rulesTitleImg} className="rules-title-back"/>
                        </div>
                        <RulesContest />
                        <img src={rulesBackImg} className="rules-bottom"/>
                </div>
                <div className="bottom"></div>
            </div>
        );
    };
    // 统计
    statistics (eventname, action = 'click', optLabel = '', optValue = '', op = '') {
        try{
            window._czc.push(['_trackEvent', eventname, action, optLabel, optValue]);
        }catch(e){

        };
    };
    // 停留时间统计
    timeSave (t) {
        if (t >= 120) {
            return;
        };
        console.log('规则页')
        this.timer = setTimeout(() => {
            this.statistics(`拆礼盒-规则页->停留大于${t}秒`);
            clearTimeout(this.timer);
            this.timeSave(t + 10);
        }, 10000);
    };
};

export default Rules;