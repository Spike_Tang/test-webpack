import Rules from './index.jsx';

export default (store) => ({
    path: '/rules',
    component: Rules,
    onEnter: (params, replace) => {
    	// 验证
    	if (window.globalData.agent.isWeb) {
    		replace('/hint');
    	};
    },
    onLeave: (params, replace) => {

    }
});

