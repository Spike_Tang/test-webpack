import Results from './index.jsx';

export default (store) => ({
    path: '/results',
    component: Results,
    onEnter: (params, replace) => {
    	// 验证
    	if (window.globalData.agent.isWeb) {
    		replace('/hint');
    	};
    },
    onLeave: (params, replace) => {

    }
});

