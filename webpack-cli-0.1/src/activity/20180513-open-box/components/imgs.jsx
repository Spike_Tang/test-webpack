import React from 'react';
import {Link} from 'react-router';

// 无交互 图片组件
// className    String
// src       String
export const StaticImage = (data) => {
    let {
        className,
        src
    } = data;

    return (
        <div className={`${className || ''} global-img-default`}>
            {
            src
                ?<div className="global-img" style={{backgroundImage: `url(${src})`}}></div>
                :''
            }
        </div>
    );
};

// 带跳转 图片组件
// className    String
// src       String
// href         String
export const LinkImage = (data) => {
    let {
        className,
        src,
        href
    } = data;

    return (
        <Link className={`${className || ''} global-img-default`} href={href} >
            {
            src
                ?<div className="global-img" style={{backgroundImage: `url(${src})`}}></div>
                :''
            }
        </Link>
    );
};

// 点击 图片组件
// className    String
// src          String
// callback     function
// callbackData array
export const ClickImage = (data) => {
    let {
        className,
        src,
        callback,
        callbackData
    } = data;

    return (
        <Link className={`${className || ''} global-img-default`} onClick={() => callback(...callbackData)}>
            {
            src
                ?<div className="global-img" style={{backgroundImage: `url(${src})`}}></div>
                :''
            }
        </Link>
    );
};