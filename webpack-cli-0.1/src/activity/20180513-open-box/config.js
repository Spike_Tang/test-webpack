/*
    rootBuildPath: 文件打包后的根目录，
    buildFilePath: 文件打包后根目录下的子目录名
    test_publicPath: 测试环境下的存放路径
    pro_publicPath: 正式环境下的存放路径
    test_outputNameHasHash: 测试环境下打包文件是否有hash
    pro_outputNameHasHash: 正式环境下打包文件是否有hash
    hasDllReferencePlugin: true 启用dll-plugin 插件
    vendor: 需要添加至静态文件的模块、框架、或者文件
    providePlugin: 自动引入文件，但是至少在项目index.js中import一次不然会造成重复打包
    hasCommonsChunkPlugin: true 启用commons-chunk-plugin 插件
    port: 设置开发环境端口
    watchOption：express静态文件服务器配置
    watchOption.publicPath：输出路径
    watchOption.filename：需要打包到指定名称时使用

    将所有js打包到一个文件中（异步文件除外）：
        hasDllReferencePlugin hasCommonsChunkPlugin都设置为false；
        设置后 CommonsChunkPlugin,DllReferencePlugin 都不会启用。

    启用单独打包静态框架：
        hasDllReferencePlugin需要设置为true，并且vendor 数组不能为空；
        设置后 DllReferencePlugin 会将vendor 中的模块打包进去；

    启用提取复用代码（hasDllReferencePlugin为false并且当框架文件、模块文件在多个文件引用时，也会将框架文件、模块文件提取到这个文件中）：
        hasCommonsChunkPlugin需要设置为true
        设置后 被多处引用的代码会打包进来，在页面初次渲染时会多请求一个 common.js

*/
const path = require('path');

module.exports = {
    buildFilePath: '/activity/20180513-open-box',
    test_publicPath: 'https://img.app.meitudata.com/c2p/app_store/mtlive/activity/20180513-open-box/',
    pro_publicPath: 'https://img.app.meitudata.com/c2p/app_store/mtlive/activity/20180513-open-box/',
    hasDllReferencePlugin: false,
    vendor: [
        'jquery',
        'react',
        'react-dom',
        'react-router',
        path.resolve(__dirname, '../../assets/adaptive.js'),
        path.resolve(__dirname, '../../assets/c2p.js')
    ],
    test_outputNameHasHash: true,
    providePlugin: {
        $: 'jquery',
        jquery: 'jquery'
    },
    hasCommonsChunkPlugin: false,
    port: 80,
    watchOption: {
        publicPath: '/c2p/app_store/finance/wallet/',
        filename: (a) => {
            return '[name].js';
        }
    }
};