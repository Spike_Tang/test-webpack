import React from 'react';
import {
    Router,
    Route,
    hashHistory,
} from 'react-router';

function getDeviceInformation() {
    let userAgent = navigator.userAgent;
    let agent = {};
    if (/meitu/.test(userAgent)) {
        agent.isMTnative = true;
    }else{
        agent.isMTnative = false;
    };

    if (/iPhone/.test(userAgent)) {
        agent.isIPhone = true;
        agent.isAndroid = false;
    }else{
        agent.isIPhone = false;
        agent.isAndroid = true;
    };

    if (/Android|webOS|iPhone|iPod|iPad|BlackBerry/i.test(userAgent)) {
        agent.isWeb = false;
    }else{
        agent.isWeb = true;
    };

    if (/QQ/.test(userAgent)) {
        agent.isQQ = true;
    }else{
        agent.isQQ = false;
    };

    return agent;
};

window.globalData = {};
window.globalData.agent = getDeviceInformation();

class App extends React.Component {
    constructor (props) {
        super(props);
    };

    render() {
        return (
            <div className="app">
                {this.props.children}
            </div>
        );
    };
};

export default App;