import React from 'react';
import {
    Router,
    Route,
    IndexRoute,
    Link,
    hashHistory
} from 'react-router';

import App from './app.js';
import {IndexHome, Home} from '../views/home/route.jsx';
import Hint from '../views/hint/route.jsx';
import Results from '../views/results/route.jsx';
import Rules from '../views/rules/route.jsx';

const routes = {
    path: '/',
    component: App,
    indexRoute: IndexHome(),
    childRoutes: [
        Home(),
        Hint(),
        Results(),
        Rules()
    ]
};

class Routes extends React.Component {
    constructor (props) {
        super(props);
    };

    render() {
        return (
            <Router history={ hashHistory } >
                { routes }
            </Router>
        );
    };
};

export default Routes;
