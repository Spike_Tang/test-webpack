import './style/reset.css';
import './style/common.scss';

import 'assetsPath/MTJs.min.js';
import 'assetsPath/adaptive.js';
import 'jquery';
// import 'assetsPath/c2p.js';

import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes/routes.js';


let Header = (props) => {
    return (
        <div className="header">test</div>
    );
};

ReactDOM.render(
    <Header />,
	document.getElementById('root')
);

// 热替换HMR，需要加入这段代码才会进行生效
if(module.hot){
    module.hot.accept();
};