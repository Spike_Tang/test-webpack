export const addZeroToStartPosition = (str) => {
	return str.toString().length < 2? '0' + str: str;
};
// 相同为 false 不同为 true
export const differentiateTwoJSON = (json1, json2) => {
	return JSON.stringify(json1) !== JSON.stringify(json2);
};
//按字节截取字符串
export const sliceStringAccordingToByte = function(string, byteNum) {
    if (isNaN(Number(byteNum)) || typeof string == 'undefined') {
        return string;
    };
    string = string.toString();
    var maxlength = Number(byteNum);
    var length = string.replace(/[^\x00-\xff]/g, '**').length;
    if (length <= maxlength) {
        return string;
    };
    var num = 0;
    var sliceStr = '';
    for (var i = 0; i < string.length; i++) {
        var addnum = 0;
        if (/[^\x00-\xff]/g.test(string[i])) {
            addnum = 2;
        } else {
            addnum = 1;
        };
        if (num + addnum > maxlength) {
            sliceStr = string.slice(0, i) + '...';
            break;
        } else {
            num += addnum;
        };
    };
    return sliceStr;
};