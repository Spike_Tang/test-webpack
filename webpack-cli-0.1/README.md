# npm命令
1. npm run main 
	1. 启动项目所用命令
	2. npm run main xxx xxx 命令后跟2个参数：执行的环境、执行的目录路径。
	3. 环境参数有 dev(开发环境) test(测试打包环境) build(生产打包环境)
	4. 目录路径 例如 项目的入口index.js地址是 src/activity/hot-battle/index.js 那么目录路径填 /activity/hot-battle
	5. 例子： sudo npm run main dev /activity/hot-battle  
2. npm run dll
  	1. 将静态框架等打包到一个文件（一般不需要单独执行，除非打包后又修改过静态文件代码）
  	2. npm run dll xxx  命令后跟1个参数： 执行的目录路径。
  	3. 目录路径 例如 项目的入口index.js地址是 src/activity/hot-battle/index.js 那么目录路径填 /activity/hot-battle
  	4. 例子：npm run dll /activity/hot-battle 