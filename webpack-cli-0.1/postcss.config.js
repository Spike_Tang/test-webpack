console.log('/* ----------------------------postcss------------------------------------------ */')
module.exports = {
    plugins: [
        require('precss'),
        require('autoprefixer')({
        	browsers : [
        		'cover 99.5%',
        		'Firefox > 40',
        		'ie > 9'
        	]
        })
    ]
};