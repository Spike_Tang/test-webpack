const path = require('path')

module.exports = (data) => {
  // const {
  //   buildFilePath
  // } = data

  return {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader'
      }, {
        loader: 'eslint-loader',
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      }]
    }, {
      test: /\.css$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader',
        options: {
          modules: true
        }
      }/*, {
        loader: 'postcss-loader'
      }*/]
    }, {
      test: /\.scss$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader'
      }, /*{
        loader: 'postcss-loader'
      },*/ {
        loader: 'sass-loader'
      }]
    }, {
      test: /\.less$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader',
        options: {
          modules: true
        }
      }, {
        loader: 'less-loader'
      }]
    }, {
      test: /\.(png|svg|jpg|gif|jpeg)$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 2048
        }
      }]
    }, {
      test: /\.(woff|woff2|eot|ttf|otf|mp4)$/,
      use: [{
        loader: 'url-loader',
        options: {}
      }]
    }, {
      test: /\.(ejs)$/,
      use: ['ejs-loader']
    }]
  }
}
