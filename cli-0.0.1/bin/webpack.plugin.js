const path = require('path')
const fs = require('fs')
const webpack = require('webpack')

const CleanWebpackPlugin = require('clean-webpack-plugin')
const UglifyESPlugin = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// 验证对象不为空
function checkoutJSON (data) {
  let arr = Object.keys(data)
  if (arr.length === 0) {
    return false
  };
  return true
};

module.exports = (data) => {
  const {
    buildPath,
    test_publicPath,
    pro_publicPath,
    environment,
    projectPath,
    hasCommonsChunkPlugin,
    hasDllReferencePlugin,
    providePlugin
  } = data

  let plugin = []
  if (environment === 'dev') {
    // 开发环境
    let devPlugin = [
      // 切换环境,react模块的根据  process.env.NODE_ENV === 'development' 使用开发版
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('development')
        }
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        template: path.join(projectPath, './index.ejs')
      })
    ]
    plugin = plugin.concat(devPlugin)
  } else if (environment === 'watch') {
  } else if (environment === 'test') {
    // 测试环境
  } else if (environment === 'build') {
    // 生产环境
    let buildPlugin = [
      // 切换环境,react模块的根据  process.env.NODE_ENV === 'production' 使用生产版
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        }
      }),
      new webpack.optimize.ModuleConcatenationPlugin(),
      // new UglifyESPlugin({
      //   uglifyOptions: {
      //     compress: {
      //       warnings: false,
      //       drop_console: true,
      //       collapse_vars: true,
      //       reduce_vars: true
      //     },
      //     output: {
      //       beautify: false,
      //       comments: false
      //     }
      //   }
      // }),
      new CleanWebpackPlugin([buildPath], {
        exclude: [
          'vendor.js',
          'vendor.manifest.json'
        ],
        allowExternal: true// 允许清理除了根目录之外的
      }),
      new HtmlWebpackPlugin()
    ]
    plugin = plugin.concat(buildPlugin)
  };

  return plugin
}
