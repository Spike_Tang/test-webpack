module.exports = (data) => {
  const {
    buildPath,
    test_publicPath,
    pro_publicPath
  } = data
  console.log(data)
  return {
    path: buildPath,
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js',
    publicPath: pro_publicPath
  }
}
