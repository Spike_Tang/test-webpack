const path = require('path')
// const fs = require('fs')

// const express = require('express')
// const myApp = express()
// const readlineSync = require('readline-sync')

const webpack = require('webpack')
const merge = require('webpack-merge')
const WebpackDevServer = require('webpack-dev-server')
// const colors = require('colors')

run()
function run () {
  // 全局变量名
  const {
    environmentsName
  } = require('./config.js')
  const defaultConfig = require('./base.config.js')
  const {
    rootBuildPath: defRootBuildPath
  } = defaultConfig
  const argvs = process.argv

  let projectConfig

  // 获取和判断输入参数
  let environment = argvs[2] || ''
  let projectPath = argvs[3] || ''
  if (!environment || !projectPath) {
    return console.log('参数不全')
  }
  if (environmentsName.find(item => item === environment) === undefined) {
    return console.log('没有这个环境')
  }
  try {
    projectPath = path.resolve(__dirname, `../src${projectPath}`)
    projectConfig = require(`${projectPath}/config.js`)
  } catch (e) {
    return console.log('没有config.js文件')
  }

  // 获取配置
  let {
    buildFilePath: proBuildFilePath = ''
  } = projectConfig
  let buildPath = path.join(defRootBuildPath, proBuildFilePath)
  // 合并项目配置 和 默认配置
  let config = merge(defaultConfig, projectConfig)
  config.buildPath = buildPath
  config.environment = environment
  config.projectPath = projectPath

  console.log('开始运行')
  // 传入配置想 组合生成 webpack.config
  const webpackConfig = {
    mode: 'none',
    entry: {
      index: path.join(projectPath, config.entryJsName)
    },
    output: require('./webpack.output.js')(config),
    module: require('./webpack.module.js')(config),
    plugins: require('./webpack.plugin.js')(config),
    devtool: require('./webpack.server.js')(config).devtool,
    resolve: require('./webpack.resolve.js')(config)
  }
  const devOptions = require('./webpack.server.js')(config).devServer
  console.log('开始判断环境')
  if (environment === 'dev') {
    console.log('本地开发环境')
    // webpackDevServer.addDevServerEntrypoints 需要在 webpack(webpackConfig) 之前
    WebpackDevServer.addDevServerEntrypoints(webpackConfig, devOptions)
    const compiler = webpack(webpackConfig)
    const server = new WebpackDevServer(compiler, devOptions)
    server.listen(devOptions.port, devOptions.host, (err) => {
      console.log(`dev server listening on port ${devOptions.port}`)
    })
  } else if (environment === 'watch') {
    console.log('连调环境')
  } else {
    console.log('生产环境或者测试环境')
    webpack(webpackConfig).run((err, state) => {
      if (err) {
        return console.log(err)
      }
      console.log(state.toString({
        chunks: true,
        chunkModules: true,
        colors: true
      }))
    })
  }
  console.log('运行结束')
}


