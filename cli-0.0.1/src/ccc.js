class A {
  constructor (time) {
    this.time = time
    console.log(this.time)
  }

  fnA () {
    console.log('a', this.time)
  }
}

class B extends A {
  constructor () {
    let a = super(10)
    console.log('123', a)
  }

  fnB () {
    console.log('B', this)
  }
}

let b = new B()
