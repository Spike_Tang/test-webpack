console.log('a.js')
let a = 'abcde'
export default a



// key value rule isPass
console.log('// ------------------------------ start ---------------------------------------')
// let formData = {
//   name: {
//     key: 'name',
//     _value: '',
//     isPass: false,
//     msg: '长度必须大于6并且小于12',
//     set value (val) {
//       console.log(`set ${this.key}`)
//       this._value = val

//       if (val.length < 6 || val.length > 12) {
//         this.isPass = false
//       } else {
//         this.isPass = true
//       }
//     },
//     get value () {
//       console.log(`get ${this.key}`)
//       return this._value
//     }
//   },
//   isChecked: {
//     key: 'isChecked',
//     _value: false,
//     isPass: false,
//     msg: '请选中',
//     set value (val) {
//       console.log(`set ${this.key}`)
//       this._value = val

//       this.isPass = val
//     },
//     get value () {
//       console.log(`get ${this.key}`)
//       return this._value
//     }
//   }
// }

// let formFns = {
//   formEach (callback) {
//     Object.keys(this).forEach((item, index, arr ) => {
//       console.log('formEach', item)
//       console.log(this)
//       console.log(this[item]['isPass'])
//       // callback(item, this[item], this[item]['isPass'], this)
//     })
//   }
// }

// let formObj = Object.assign(Object.create(formFns), formData)

// let formHandler = {
//   set (target, prop, value, receiver) {
//     target[prop]['value'] = value
//     return true
//   },
//   get (target, prop, receiver) {
//     console.log('handler get', prop)
//     console.log(target[prop])
//     if (target[prop] instanceof Function){
//       return target[prop]
//     } else if (target[prop] instanceof Object) {
//       return target[prop]['value']
//     }
//   }
// }

// let form = new Proxy(formObj, formHandler)

// form.formEach((key, value, isPass, obj) => {
//   console.log([key, value, isPass, obj])
//   console.log('// ------------------------------ 分割 ---------------------------------------')
// })

// let ajaxData = {
//   name: 'hi',
//   age: 18,
//   isChecked: true
// }

// Object.assign(form, ajaxData)
// console.log(form, formData)
/*
name  ''
age  ''
isChecked..''
*/

/*
var handler = {
  get: function (target, name) {
    console.log(target, name)
    console.log('2')
    if (name === 'prototype') {
      return Object.prototype
    }
    return 'Hello, ' + name
  },

  apply: function (target, thisBinding, args) {
    console.log(target, thisBinding, args)
    console.log('3')
    return args[0]
  },

  construct: function (target, args) {
    console.log(target, args)
    console.log('4')
    return {value: args[1]}
  }
}

var Fproxy = new Proxy(function (x, y) {
  console.log('0')
  return x + y
}, handler)

let a1 = Fproxy(1, 2)
console.log(a1)
console.log('// ------------------------------ 分割 ---------------------------------------')
let a2 = new Fproxy(1, 2)
console.log(a2)
console.log('// ------------------------------ 分割 ---------------------------------------')
let a3 = Fproxy.prototype === Object.prototype
console.log(a3)
console.log('// ------------------------------ 分割 ---------------------------------------')
let a4 = Fproxy.foo === 'Hello, foo'
console.log(a4)
console.log('// ------------------------------ 分割 ---------------------------------------')*/
