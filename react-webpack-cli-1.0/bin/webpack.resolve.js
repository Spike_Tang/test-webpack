const path = require('path');

module.exports = (data) => {
    const {
        buildFilePath
    } = data;

    return {
        alias:{
            assetsPath: path.resolve(__dirname, `../src/assets`),
            stylePath: path.resolve(__dirname, `../src/${buildFilePath}/style`),
            imgPath: path.resolve(__dirname, `../src/${buildFilePath}/style/img`),
            componentPath: path.resolve(__dirname, `../src/${buildFilePath}/components`)
        },
        extensions: ['.jsx', '.js', '.json', 'css', 'scss']
    };
};

