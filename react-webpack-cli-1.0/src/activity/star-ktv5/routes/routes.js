import React from 'react';
import {
    Router,
    Route,
    IndexRoute,
    Link,
    hashHistory
} from 'react-router';

import App from './app.js';
import {IndexHome, Home} from '../views/home/route.jsx';

const routes = {
    path: '/',
    component: App,
    indexRoute: IndexHome(),
    childRoutes: [
        Home(),
    ]
};

const Routes = () => {
    return (
        <Router history={ hashHistory }>
            { routes }
        </Router>
    );
};

export default Routes;
