export const IndexHome = (store) => ({
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./index.jsx').default)
        }, 'home')
    }
});

export const Home = (store) => ({
    path: '/home',
    getComponent(nextState, cb) {
        require.ensure([], (require) => {
            cb(null, require('./index.jsx').default)
        }, 'home')
    }
});

